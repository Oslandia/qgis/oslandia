# Installation

## Stable version (recomended)

This plugin is published on the official QGIS plugins repository: <https://plugins.qgis.org/plugins/oslandia/>.

## Beta versions released

Enable experimental extensions in the QGIS plugins manager settings panel.

## Earlier development version

If you define yourself as early adopter or a tester and can't wait for the release, the plugin is automatically packaged for each commit to main, so you can use this address as repository URL in your QGIS extensions manager settings:

```url
https://oslandia.gitlab.io/qgis/oslandia/plugins.xml
```

Dans QGIS :

1. Menu `Extensions` > `Installer/Gérer des extensions`
2. Onglet `Paramètres`
3. Sous la liste des dépôts, cliquer sur `Ajouter...` et renseigner l'URL ci-dessus
4. Une fois le dépôt ajouté, l'extension devrait apparaître dans l'onglet `Non installées`.

:::{warning}
Selon votre configuration, redémarrer QGIS peut être nécessaire, le gestionnaire d'extensions ayant des comportements parfois capricieux par rapport aux dépôts tiers.
:::
