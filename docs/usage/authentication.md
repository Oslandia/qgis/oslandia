
# Pairing the plugin with your GitLab account

## Introduction

To access the GitLab API and manage your assistance project directly within QGIS, we need to configure an authorized application in GitLab and use it in QGIS.

:::{info}
OAuth2 is an authorization framework that allows applications to obtain limited access to user accounts on an HTTP service.  
GitLab uses OAuth2 for authentication and authorization, enabling third-party applications to access GitLab resources on behalf of a user.
:::

## JSON Configuration

To create authentication in QGIS, we use an OAuth2 configuration file in JSON format. It must at least look like this:

```json
{
    "accessMethod": 0,
    "grantFlow": 0,
    "configType": 1,
    "clientId": null,
    "clientSecret": null,
    "requestUrl": "https://git.oslandia.net/oauth/authorize",
    "tokenUrl": "https://git.oslandia.net/oauth/token",
    "redirectPort": 7070,
    "redirectUrl": "callback",
    "persistToken": false,
    "requestTimeout": 30,
    "version": 1,
    "scope": "api"
}
```

To provide this file, you have two options:

### 1. Provide the location using an environment variable

This environment variable is: `PLUGIN_OSLANDIA_OAUTH2_CONFIG`

### 2. Otherwise, the default file provided with the plugin will be used

The default file is located within the plugin.

## Connecting through the Interface

To connect, proceed through the interface:

1. Click the connection button in the toolbar.  
    ![](../static/img/connect.png)

2. This window will open. Click the Connect button.  
    ![](../static/img/authent_dialog.png)

3. The web browser will open. Enter your credentials.  
    ![](../static/img/browser_authent.png)

4. You are connected. You can access the user interface:  
    ![](../static/img/user.png)  
    ![](../static/img/user_information.png)
