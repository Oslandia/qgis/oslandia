# Using this plugin with QGIS Deployment Toolbelt (QDT)

If you want to use this plugin with [QGIS Deployment Toolbelt (QDT)](https://qgis-deployment.github.io/qgis-deployment-toolbelt-cli/), you can add the following snippet to your `profile.json` file, under the `plugins` attribute:

```json
    {
      "name": "Oslandia",
      "folder_name": "oslandia",
      "official_repository": true,
      "plugin_id": 3501,
      "version": "0.4.1"
    }
```

Remember to replace the `version` attribute with the version you want to install.
