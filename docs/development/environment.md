# Development

## Environment setup

Typically on Ubuntu:

```bash
# create virtual environment linking to system packages (for pyqgis)
python3 -m venv .venv --system-site-packages
source .venv/bin/activate

# bump dependencies inside venv
python -m pip install -U pip
python -m pip install -U -r requirements/development.txt

# install git hooks (pre-commit)
pre-commit install
```

### Dedicated QGIS profile

It's recommended to create a dedicated QGIS profile for the development of the plugin to avoid conflicts with other plugins.

```sh
qgis --profile plg_oslandia
```

### Manage oAuth2 authentication

> Commands to be executed from the local clone root folder. Tested on modern shells, like Bash or PowerShell.

To manage an oAuth2 authentication dedicated to the development:

1. Create a local folder to store the authentication JSON file. Let's say:

    ```sh
    mkdir -p ~/.oslandia/auth
    ```

1. Copy the authentication JSON file to the local folder:

    ```sh
    cp ./oslandia/auth/oauth2_config.json ~/.oslandia/auth/oauth2_config.json
    ```

1. Get credentials ready:
    - either donwloading a public version of the plugin from <https://plugins.qgis.org/plugins/oslandia/>
    - or getting it from the password manager

1. Add the following environment variable `PLUGIN_OSLANDIA_OAUTH2_CONFIG` to the QGIS profile dedicated to your development pointing to the JSON file:

    ![QGIS - Environment variables](../static/img/qgis_env_vars_PLUGIN_OSLANDIA_OAUTH2_CONFIG.png)

1. Restart QGIS
