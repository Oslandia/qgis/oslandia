# Manage translations

## Requirements

Qt Linguist tools are used to manage translations. Typically on Ubuntu:

```bash
sudo apt install qttools5-dev-tools
```

## Workflow

1. Generate the `plugin_translation.pro` file:

    ```bash
    python oslandia/resources/i18n/generate_pro.py
    ```

1. Update `.ts` files:

    ```bash
    pylupdate5 -noobsolete -verbose oslandia/resources/i18n/plugin_translation.pro
    ```

1. Translate your text using QLinguist or directly into `.ts` files.
1. Compile it:

    ```bash
    lrelease oslandia/resources/i18n/*.ts
    ```
