# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Installation <usage/installation>
Authentication <usage/authentication>
usage/with_qdt
Generate json files describing plugins <usage/generate_plugins_json>
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
Code documentation <_apidoc/oslandia>
```
