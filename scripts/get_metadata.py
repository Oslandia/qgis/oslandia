import logging
from configparser import ConfigParser, ParsingError
from typing import Optional
from urllib.parse import quote, urlparse

import requests
import toml

GITLAB_API_URL = "/api/v4/projects/"
GITHUB_API_URL = "https://api.github.com/repos/"


class PluginMetadataRetriever:
    def __init__(self, http_session: requests.Session, repo_url: str) -> None:
        self.http_session = http_session
        self.repo_url = urlparse(repo_url)
        self.default_branch = None

    @property
    def get_repo_url(self) -> str:
        """Return repo_url as str

        :return: repo url
        :rtype: str
        """
        return self.repo_url.geturl()

    @property
    def get_default_branch(self) -> Optional[str]:
        """Detects the default branch of a GitHub or GitLab repository using their APIs

        :param repo_url: Repository URL (GitHub or GitLab)
        :type repo_url: str
        :return: Name of the default branch
        :rtype: str
        """
        if not self.default_branch:
            try:
                if "gitlab" in self.get_repo_url:
                    self.default_branch = self._get_gitlab_default_branch()
                elif "github" in self.get_repo_url:
                    self.default_branch = self._get_github_default_branch()
                else:
                    logging.error(f"Unsupported repository host: {self.get_repo_url}")
                    return None
            except Exception as e:
                logging.error(
                    f"Error detecting default branch for {self.get_repo_url}: {str(e)}"
                )
                return None
        return self.default_branch

    @property
    def _detect_gitlab_instance(self) -> str:
        """Detect Gitlab instance url

        :return: Gitlab instance url
        :rtype: str
        """
        instance_url = f"{self.repo_url.scheme}://{self.repo_url.netloc}"
        return instance_url

    def _get_gitlab_default_branch(self) -> str:
        """Get default branch from GitLab API

        :return: Gitlab repo URL
        :rtype: str
        """
        # Extract project path from URL
        path = self.repo_url.path.strip("/")
        api_url = self._detect_gitlab_instance + GITLAB_API_URL + quote(path, safe="")

        response = self.http_session.get(api_url)

        if 200 <= response.status_code < 300:
            default_branch = response.json().get("default_branch")
            logging.info(
                f"GitLab default branch of {self.get_repo_url} is {default_branch}"
            )
            return default_branch
        else:
            logging.error(f"GitLab API error with {api_url}: {response.status_code}")
            return "nc"

    def _extract_owner_and_repo_on_github_url(self):
        """Extract owner and repository from the GitHub URL."""
        path_parts = self.repo_url.path.strip("/").split("/")

        if len(path_parts) == 2:
            return path_parts[0], path_parts[1]  # owner, repo
        else:
            logging.error(f"Invalid GitHub URL: {self.repo_url}")
            return None, None

    def _get_github_default_branch(self) -> str:
        """Get default branch from Github API

        :return: Github repo URL
        :rtype: str
        """
        owner, repo = self._extract_owner_and_repo_on_github_url()
        if not owner and not repo:
            return "nc"

        api_url = f"{GITHUB_API_URL}{quote(owner.encode())}/{quote(repo.encode())}"

        headers = {"Accept": "application/vnd.github.v3+json"}
        response = self.http_session.get(api_url, headers=headers)

        if 200 <= response.status_code < 300:
            default_branch = response.json().get("default_branch")
            logging.info(
                f"Github default branch of {self.get_repo_url} is {default_branch}"
            )
            return default_branch
        else:
            logging.error(f"GitHub API with {api_url}: {response.status_code}")
            return "nc"

    def file_exists_in_branch(self, file_name: str) -> bool:
        """Check if a specific file exists in a given branch of a GitLab repository.

        :param repo_url: base URL of the repository (e.g., "https://gitlab.com/user/repository/").
        :type repo_url: str
        :param branch: branch to check for the existence of the file (e.g., "main" or "master").
        :type branch: str
        :param file_name: file name to check for (e.g., "pyproject.toml" or "setup.cfg").
        :type file_name: str

        :return: True if the file exists, False otherwise.
        :rtype: bool
        """

        file_url = self.build_git_raw_file_url(file_name)

        response = self.http_session.head(file_url, allow_redirects=True)

        if response.status_code == 200:
            logging.info(
                f"In {self.get_repo_url} git repository the file {file_name} exists in branch {self.get_default_branch}"
            )
            return True
        elif response.status_code == 404:
            logging.info(
                f"In {self.get_repo_url} git repository the file {file_name} does not exists in branch {self.get_default_branch}"
            )
            return False
        else:
            logging.error(
                f"In {self.get_repo_url} unexpected response code {response.status_code} for file {file_name} in"
                f"branch {self.get_default_branch}. Requested URL: {file_url}",
            )
            return False

    def find_plugin_path_in_setup_cfg(self, setup_url: str) -> Optional[str]:
        """Searches for the 'plugin_path' key in any section of the setup.cfg file.

        :param setup_url: Url to the setup.cfg file
        :type file_path: str
        :return: Value of 'plugin_path' if found, otherwise None
        :rtype: Optional[str]
        """
        response = self.http_session.get(setup_url)
        response.encoding = "utf-8"
        config = ConfigParser()
        config.read_string(response.text)

        for section in config.sections():
            if "plugin_path" in config[section]:
                return config[section]["plugin_path"]

        return None

    def find_plugin_path_in_pyproject_toml(self, toml_url: str) -> Optional[str]:
        """Searches for the 'plugin_path' key in any section of the pyproject.toml file.

        :param toml_url: toml url to the pyproject.toml file
        :type file_path: str
        :return: Value of 'plugin_path' if found, otherwise None
        :rtype: Optional[str]
        """
        response = self.http_session.get(toml_url)
        response.encoding = "utf-8"
        if 200 <= response.status_code < 300:
            toml_content = response.text
            data = toml.loads(toml_content)

        def search_dict_for_key(d: dict, key: str) -> Optional[str]:
            """Recursively searches for a key in a nested dictionary."""
            for k, v in d.items():
                if isinstance(v, dict):
                    result = search_dict_for_key(v, key)
                    if result:
                        return result
                elif k == key:
                    return v
            return None

        return search_dict_for_key(data, "plugin_path")

    def build_git_raw_file_url(self, filename: str) -> str:
        """Build a file url for github or gitlab from a base url, a branch and a file path in the repo.

        :param url: Base url (exemple: "https://github.com/aeag/MenuFromProject-Qgis-Plugin")
        :type url: str
        :param branch: Branch (exemple: "master")
        :type branch: str
        :param filename: File name and path in the repo (exemple: "readme.txt" ou "toto/folder/my_file.txt")
        :type filename: str
        :return: Correctly constructed URL
        :rtype: str
        """
        if "gitlab" in self.get_repo_url:
            return f"{self.get_repo_url}-/raw/{self.get_default_branch}/{filename}?ref_type=heads"
        elif "github" in self.get_repo_url:
            return (
                f"{self.get_repo_url}refs/heads/{self.get_default_branch}/{filename}"
            ).replace("github.com", "raw.githubusercontent.com")
        else:
            return ""

    def get_metadata(self) -> dict:
        """Get metadata from a plugin repository URL.

        :param url: Plugin repository URL
        :type url: str
        :return: Plugin metadata dictionary
        :rtype: dict
        """
        metadata_fields = [
            "name",
            "version",
            "about",
            "description",
            "tags",
            "repository",
            "homepage",
            "experimental",
            "description_fr",
            "about_fr",
            "icon",
            "funders",
            "screenshot",
            "issue_to_be_funded",
            "blog_link",
        ]
        result = {field: None for field in metadata_fields}

        default_branch = self.get_default_branch
        if not default_branch:
            logging.error(
                f"{self.get_repo_url}: default branch could not be determined."
            )
            return result

        # Determine plugin path
        plugin_path = None
        for config_file, finder in [
            ("setup.cfg", self.find_plugin_path_in_setup_cfg),
            ("pyproject.toml", self.find_plugin_path_in_pyproject_toml),
        ]:
            if self.file_exists_in_branch(config_file):
                plugin_path = finder(self.build_git_raw_file_url(config_file))
                if plugin_path:
                    break

        if not plugin_path:
            logging.warning(
                f"{self.get_repo_url}: no valid plugin_path found in setup.cfg or pyproject.toml."
            )
            return result

        logging.info(f"{self.get_repo_url}: plugin subfolder path: {plugin_path}")

        # Check metadata.txt in possible locations
        metadata_urls = [
            self.build_git_raw_file_url(f"{plugin_path}/metadata.txt"),
            self.build_git_raw_file_url("metadata.txt"),
        ]

        metadata_content = None
        for metadata_url in metadata_urls:
            response = self.http_session.get(metadata_url)
            if response.status_code == 200:
                metadata_content = response.text
                break

        if not metadata_content:
            logging.error(
                f"{self.get_repo_url}: metadata file not found or inaccessible."
            )
            return result

        try:
            metadata = ConfigParser(interpolation=None)
            metadata.read_string(metadata_content)
        except ParsingError:
            logging.error(f"{self.get_repo_url}: error parsing metadata.txt.")
            return result

        # Extract metadata fields
        for field in metadata_fields:
            result[field] = self.iter_sections_in_config_parser(metadata, field)

        # Handle special cases (URLs and icons)
        for url_field in ["repository", "homepage", "icon"]:
            if result[url_field]:
                result[url_field] = self.clean_up_url_case_error(result[url_field])

        if result["icon"]:
            result["icon"] = self.build_git_raw_file_url(
                f"{plugin_path}/{result['icon']}"
            )

        return result

    def iter_sections_in_config_parser(self, config: ConfigParser, key: str):
        """Search for a key in the 'oslandia' and 'general' sections of a ConfigParser object.

        :param config: ConfigParser
        :type config: ConfigParser
        :param key: Key to search for in the .ini file
        :type key: str

        :return: Key value if found, otherwise empty string
        :rtype: str
        """
        res = ""

        for section in ["oslandia", "general"]:
            if config.has_section(section) and config.has_option(section, key):
                res = config.get(section, key)
                break

        return res

    def clean_up_url_case_error(self, url: str) -> str:
        """Corrects case-sensitivity errors in specific URLs based on predefined mappings.

        This function takes a URL and checks if it contains any substrings that are known to have case errors.
        If a match is found, it replaces the incorrect substring with the correct one. Otherwise, it returns
        the URL unchanged.

        :param url: The URL to check and potentially correct.
        :type url: str
        :return: The corrected URL if a case error is found; otherwise, the original URL.
        :rtype: str
        """
        corrections = {
            "oslandia/QGIS": "Oslandia/qgis",
            "Oslandia/QGIS": "Oslandia/qgis",
        }
        for incorrect, correct in corrections.items():
            if incorrect in url:
                return url.replace(incorrect, correct)
        return url
