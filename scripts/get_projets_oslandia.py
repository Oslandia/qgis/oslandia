"""
The purpose of this script is to parse a json file that describes the plugins oslandia has presented.

It is based on 3 sources :

- 1. Retrieving plugins authored by oslandia (or an oslandia member) from the QGIS plugins xml passing the current QGIS version (for example https://plugins.qgis.org/plugins/plugins.xml?qgis=3.34)
- 2. Scanning plugins in the GitLab Oslandia
- 3. Read a remote JSON file containing private plugins.
"""

import argparse
import json
import logging
import sys
import time
import xml.etree.ElementTree as ET
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import lru_cache
from pathlib import Path
from typing import Optional

import requests
from packaging import version

sys.path.append(str(Path(__file__).resolve().parent.parent))
from scripts.get_metadata import PluginMetadataRetriever

http_session: Optional[requests.Session] = None


@lru_cache
def url_trailing_slash(url: str) -> str:
    """Returns the url with a trailing slash if it doesn't already have one.

    :param url: The URL to check.
    :type url: str

    :return: The URL with a trailing slash.
    :rtype: str
    """
    if isinstance(url, str):
        return url if url.endswith("/") else url + "/"
    logging.error(f"Invalid URL: {url}")


def set_http_session() -> None:
    """Initializes the global HTTP session if it has not been set."""
    global http_session
    if not http_session:
        http_session = requests.Session()


# Source 1 : from qgis.plugins.org XML
def get_xml_plugin() -> str:
    """Returns the xml (in str) of plugins with oslandia as author

    :return: XML of plugins oslandia
    :rtype: str
    """

    url = "https://plugins.qgis.org/plugins/plugins.xml?qgis=ltr"
    response = http_session.get(url)
    response.raise_for_status()  # Raise an error for bad status codes
    response.encoding = "utf-8"
    xml_data = response.text

    # Parse XML
    root = ET.fromstring(xml_data)

    # Create a new root element for the filtered plugins
    filtered_root = ET.Element("plugins")

    for plugin in root.findall("pyqgis_plugin"):
        author_element = plugin.find("author_name")
        if author_element is not None:
            author = author_element.text
            if author and "oslandia" in author.lower():
                filtered_root.append(plugin)

    filtered_xml_str = ET.tostring(filtered_root, encoding="unicode")
    return filtered_xml_str


def fetch_plugin_metadata(code_url: str) -> dict:
    """
    Retrieves plugin metadata from its URL.
    :param http_session: HTTP session used for the request.
    :param code_url: URL of plugin source code.
    :return: Dictionary containing plugin metadata.
    """
    if any(platform in code_url for platform in ["gitlab", "github"]):
        try:
            return PluginMetadataRetriever(http_session, code_url).get_metadata()
        except Exception as e:
            logging.error(f"Error retrieving metadata from {code_url}: {e}")

    return {
        "name": None,
        "version": None,
        "about": None,
        "file_name": None,
        "experimental": None,
        "description": None,
        "tags": None,
        "repository": None,
        "homepage": None,
        "description_fr": None,
        "about_fr": None,
        "icon": None,
        "funders": None,
        "screenshot": None,
        "issue_to_be_funded": None,
        "blog_link": None,
    }


def fetch_all_metadata(plugins: list[tuple[str, str]]) -> dict:
    """
    Retrieves metadata from several plugins in parallel.

    :param http_session: HTTP session used for requests.
    :param plugins: List of tuples (name, code_url).
    :return: Dictionary of metadata per plugin.
    """
    metadata_results = {}
    with ThreadPoolExecutor(max_workers=10) as executor:
        future_to_plugin = {
            executor.submit(fetch_plugin_metadata, code_url): (
                name,
                code_url,
            )
            for name, code_url in plugins
        }

        for future in as_completed(future_to_plugin):
            name, code_url = future_to_plugin[future]
            try:
                metadata_results[code_url] = future.result()
            except Exception as e:
                logging.error(f"Erreur lors du traitement du plugin {name}: {e}")
                metadata_results[code_url] = None

    return metadata_results


def build_json_from_private_and_medatada(json_path: Path) -> dict:
    """Extracts plugin data from the private and formats it as a dictionary.

    :param json_path: Path to plugin json excluded.
    :type json_path: Path
    :return: A dictionary with plugin data formatted for JSON.
    :rtype: dict
    """
    # Parse the XML content
    plugins_list = []
    with open(json_path, encoding="utf-8") as json_file:
        data = json.load(json_file)

    plugins = [
        (plugin.get("name"), url_trailing_slash(plugin.get("code_url")))
        for plugin in data.get("private_plugins", [])
    ]

    metadata_results = fetch_all_metadata(plugins)

    for name, code_url in plugins:
        result_metadata = metadata_results.get(code_url, {})
        plugins_list.append(
            {
                "name": name,
                "version": result_metadata.get("version"),
                "plugin_id": "",
                "about": result_metadata.get("about"),
                "file_name": None,
                "experimental": result_metadata.get("experimental"),
                "description": {
                    "en": result_metadata.get("description"),
                    "fr": None,
                },
                "logo": result_metadata.get("icon"),
                "tags": result_metadata.get("tags"),
                "funders": result_metadata.get("funders"),
                "doc_url": url_trailing_slash(result_metadata.get("homepage")),
                "code_url": code_url,
                "screenshot": result_metadata.get("screenshot"),
                "issue_to_be_funded": result_metadata.get("issue_to_be_funded"),
                "blog_link": result_metadata.get("blog_link"),
            }
        )

    return {"private_plugins": plugins_list}


def build_json_from_xml_and_medata(xml_content: str) -> dict:
    """Extracts plugin data from the XML content and formats it as a dictionary.

    :param xml_content: The XML content as a string.
    :type xml_content: str
    :return: A dictionary with plugin data formatted for JSON.
    :rtype: dict
    """
    # Parse the XML content
    root = ET.fromstring(xml_content)

    plugins_list = []
    plugins = []
    for plugin in root.findall("pyqgis_plugin"):
        if plugin.get("name") not in get_exclude_plugins():
            name = plugin.get("name")
            code_url = url_trailing_slash(plugin.findtext("repository", default=""))
            plugins.append((name, code_url))

    metadata_results = fetch_all_metadata(plugins)

    for name, code_url in plugins:
        result_metadata = metadata_results.get(code_url, {})
        plugins_list.append(
            {
                "name": name,
                "version": result_metadata.get("version"),
                "plugin_id": "",
                "about": result_metadata.get("about"),
                "file_name": None,
                "experimental": result_metadata.get("experimental"),
                "description": {
                    "en": result_metadata.get("description"),
                    "fr": None,
                },
                "logo": result_metadata.get("icon"),
                "tags": result_metadata.get("tags"),
                "funders": result_metadata.get("funders"),
                "doc_url": url_trailing_slash(result_metadata.get("homepage")),
                "code_url": code_url,
                "screenshot": result_metadata.get("screenshot"),
                "issue_to_be_funded": result_metadata.get("issue_to_be_funded"),
                "blog_link": result_metadata.get("blog_link"),
            }
        )

    return dict_duplicate_cleaning({"public_plugins": plugins_list})


def build_json_from_gitlab_public_and_metadata() -> dict:
    """Generates the oslandia public gitlab plugin dictionary. Built from metadata files.

    :return: plugin data formatted for JSON.
    :rtype: dict
    """
    plugins_list = []
    gitlab_plugins_names, gitlab_plugins_repos_names = (
        get_plugin_in_gitlab_and_not_in_xml()
    )

    plugins = [
        (plugin_name, f"https://gitlab.com/Oslandia/qgis/{plugin_path}/")
        for plugin_name, plugin_path in zip(
            gitlab_plugins_names, gitlab_plugins_repos_names
        )
    ]

    metadata_results = fetch_all_metadata(plugins)

    for plugin_name, plugin_url in plugins:
        result_metadata = metadata_results.get(plugin_url, {})
        plugins_list.append(
            {
                "name": plugin_name,
                "version": result_metadata.get("version"),
                "plugin_id": None,
                "about": result_metadata.get("about"),
                "file_name": None,
                "experimental": result_metadata.get("experimental"),
                "description": {
                    "en": result_metadata.get("description"),
                    "fr": result_metadata.get("description_fr"),
                },
                "logo": result_metadata.get("icon"),
                "tags": result_metadata.get("tags"),
                "funders": result_metadata.get("funders"),
                "doc_url": url_trailing_slash(result_metadata.get("homepage")),
                "code_url": url_trailing_slash(result_metadata.get("repository")),
                "screenshot": result_metadata.get("screenshot"),
                "issue_to_be_funded": result_metadata.get("issue_to_be_funded"),
                "blog_link": result_metadata.get("blog_link"),
            }
        )

    return {"gitlab_plugins": plugins_list}


def get_plugins_list_from_xml(xml_content: str) -> list:
    """Extracts plugin name list from the XML in list.

    :param xml_content: XML content as a string.
    :type xml_content: str

    :return: dictionary with plugin data formatted for JSON.
    :rtype: list
    """
    # Parse the XML content
    root = ET.fromstring(xml_content)

    plugins_list = []

    # Iterate over each 'pyqgis_plugin' element
    for plugin in root.findall("pyqgis_plugin"):
        if plugin.get("name") not in get_exclude_plugins():
            plugins_list.append(plugin.get("name"))

    return plugins_list


# Source 2 : from gitlab.com/oslandia/qgis
def get_plugins_gitlab_oslandia() -> list[list]:
    """Returns two lists, a list of names and a list of plugin repository paths on the
    oslandia public gitlab.

    :return: List of lists, a list of names and a list of repository names.
    :rtype: list[list]
    """
    # oslandia/qgis group id (https://docs.gitlab.com/ee/user/group/#get-the-group-id)
    group_id = "8179541"
    api_url = f"https://gitlab.com/api/v4/groups/{group_id}/"

    response = http_session.get(api_url)
    response.raise_for_status()
    data = response.json()

    projects_names = []
    projects_paths = []
    for project in data["projects"]:
        if "oslandia-featured-qgis-plugin" in project["tag_list"]:
            projects_names.append(project["name"])
            projects_paths.append(project["path"])

    return [projects_names, projects_paths]


def get_plugin_in_gitlab_and_not_in_xml() -> list[list]:
    """Returns the same thing as get_plugins_gitlab_oslandia, but only for plugins
    present on the public gitlab oslandia and not published on the official qgis
    repository.

    :return: List of lists, a list of names and a list of repository names.
    :rtype: list[list]
    """
    paths: list = []
    names: list = []
    for plugin_name, plugin_path in zip(
        get_plugins_gitlab_oslandia()[0], get_plugins_gitlab_oslandia()[1]
    ):
        if (
            plugin_name not in get_plugins_list_from_xml(get_xml_plugin())
            and plugin_name not in get_exclude_plugins()
        ):
            names.append(plugin_name)
            paths.append(plugin_path)

    return [names, paths]


def get_exclude_plugins() -> list:
    """Retrieves the names of plugins to be excluded from a json file and renames them
    in a list.

    :return: List of plugins names
    :rtype: list
    """
    file_path = Path("oslandia/static/json_plugins/exclude_plugins.json")
    json_content = file_path.read_text()
    data = json.loads(json_content)
    return [plugin["name"] for plugin in data["exclude_plugins"]]


def save_to_json(data: dict, file_path: Path) -> None:
    """Saves the given dictionary data to a JSON file.

    :param data: data dictionary to save.
    :type data: dict
    :param file_path: output file name.
    :type file_path: Path
    """
    with file_path.open("w", encoding="utf-8") as json_file:
        json.dump(data, json_file, indent=4, ensure_ascii=False)


def dict_duplicate_cleaning(public_dict: dict) -> dict:
    """In the plugins, some are duplicates when there was an experimental version
    before a non-experimental one. We need to clean them up.

    :param public_dict: input dictionary of public plugins.
    :type public_dict: dict

    :return: deduplicated plugins dictionary
    :rtype: dict
    """
    cleaned_public_plugins = {}

    for plugin in public_dict["public_plugins"]:
        name = plugin["name"]
        current_version = plugin["version"]

        if name in cleaned_public_plugins:
            if version.parse(current_version) > version.parse(
                cleaned_public_plugins[name]["version"]
            ):
                cleaned_public_plugins[name] = plugin
        else:
            cleaned_public_plugins[name] = plugin

    public_dict["public_plugins"] = list(cleaned_public_plugins.values())

    return public_dict


def init_logging(args: argparse.Namespace, output_dir: Path) -> None:
    """Initializes logging rendering according to the parameters passed.

    :param args: Arguments
    :type args: argparse.Namespace
    :param output_dir: Ouput directory
    :type output_dir: Path
    """
    if args.verbose:
        if args.logfile:
            logging.basicConfig(
                level=logging.INFO,
                filename=output_dir.joinpath("get_plugins_oslandia.log"),
                filemode="w",
                format=log_format,
            )
        else:
            logging.basicConfig(level=logging.INFO, format=log_format)
    else:
        if args.logfile:
            logging.basicConfig(
                level=logging.WARNING,
                filename=output_dir.joinpath("get_plugins_oslandia.log"),
                filemode="w",
                format=log_format,
            )
        else:
            logging.basicConfig(level=logging.WARNING, format=log_format)


def generate_json(type_: str, output_dir: Path) -> None:
    """Handles JSON generation based on the type specified.

    This function generates the appropriate JSON file based on the specified
    'type' argument, and saves it to the specified 'output_dir'.

    :param type_: Type of JSON to generate ('xml', 'gitlab', or 'private')
    :type type_: str
    :param output_dir: Directory where the JSON files will be saved
    :type output_dir: Path
    """
    if type_ == "xml":
        json_from_xml = build_json_from_xml_and_medata(get_xml_plugin())
        save_to_json(json_from_xml, output_dir.joinpath("public_plugins.json"))

    elif type_ == "gitlab":
        json_from_gitlab_public = build_json_from_gitlab_public_and_metadata()
        save_to_json(
            json_from_gitlab_public, output_dir.joinpath("gitlab_plugins.json")
        )

    elif type_ == "private":
        json_from_private_plugin = build_json_from_private_and_medatada(
            Path("./oslandia/static/json_plugins/private_plugins.json")
        )
        save_to_json(
            json_from_private_plugin, output_dir.joinpath("private_plugins.json")
        )


# -- CLI --
if __name__ == "__main__":

    start_time = time.time()
    set_http_session()
    # variables
    log_format = (
        "%(asctime)s||%(levelname)s||%(module)s||%(funcName)s||%(lineno)d||%(message)s"
    )

    parser = argparse.ArgumentParser(description="Generate JSON files for plugins.")

    parser.add_argument(
        "-t",
        "--type",
        type=str,
        default=None,
        choices=("gitlab", "private", "xml"),
        help="Type of JSON to generate: 'xml' for public_plugins.json, "
        "'gitlab' for gitlab_plugins.json, or 'private' for private_plugins.json. "
        "If not specified, all JSON files will be generated.",
    )

    parser.add_argument(
        "-o",
        "--output",
        type=str,
        default=".",
        help="Directory where the JSON files will be saved.",
    )

    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Enable verbose logging"
    )
    parser.add_argument(
        "--logfile",
        action="store_true",
        help="Enable logging to a get_plugins_oslandia.log file",
    )

    args = parser.parse_args()
    output_dir = Path(args.output)
    output_dir.mkdir(parents=True, exist_ok=True)
    init_logging(args, output_dir)

    if args.type is None or args.type == "xml":
        json_from_xml = build_json_from_xml_and_medata(get_xml_plugin())
        save_to_json(json_from_xml, output_dir.joinpath("public_plugins.json"))

    if args.type is None or args.type == "gitlab":
        json_from_gitlab_public = build_json_from_gitlab_public_and_metadata()
        save_to_json(
            json_from_gitlab_public, output_dir.joinpath("gitlab_plugins.json")
        )

    if args.type is None or args.type == "private":
        json_from_private_plugin = build_json_from_private_and_medatada(
            Path("./oslandia/static/json_plugins/private_plugins.json")
        )
        save_to_json(
            json_from_private_plugin, output_dir.joinpath("private_plugins.json")
        )

    end_time = time.time()

    delta = end_time - start_time
    http_session.close()
    logging.info(f"The script took {delta:.2f} seconds to execute.")
