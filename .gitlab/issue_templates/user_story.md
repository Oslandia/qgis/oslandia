<!-- Provide a clear, concise title for the user story -->

## Description

<!--

Example:

As #1 and #2, I want to receive notifications about new plugin releases so that I can stay updated with the latest tools and features from Oslandia.

-->

**AS**
<!-- Link to issues corresponding to persona(s) -->


**I want to**
<!-- the user's goal and the benefit he/she will receive -->

**So that**
<!-- how it should work from the point of view of end user -->


### Acceptance Criteria
<!-- List specific criteria that must be met for the story to be complete. kind of Definition of Done (https://scrum-master.org/definition-of-done-dod-scrum-agile/)

Example:

- [ ] notifications are sent to users when a new plugin is released.
- [ ] they include the plugin name, version, and a brief description.
- [ ] users can opt-in or opt-out of receiving notifications.

-->

- [ ] XXX

### Details/Notes
<!-- Include any additional information or context that might be useful

Example:

- notifications should be non-intrusive and easy to read.

-->


### Assumptions

<!-- Any assumptions, requirements made that are relevant to the story.$


Example:

- Users have an internet connection.
- Users have the plugin installed and configured to receive updates.

 -->

- Users have an internet connection.
- Users have the plugin installed.
- XXXX

### Attachments

<!-- Attach or link to any relevant documents, designs, or screenshots. Please, upload design to... the design dedicated section below in Gitlab web interface! -->
