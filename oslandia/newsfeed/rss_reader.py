#! python3  # noqa: E265


"""
    Minimalist RSS reader.
"""

# ############################################################################
# ########## Imports ###############
# ##################################

# Standard library
import logging
import xml.etree.ElementTree as ET
from email.utils import parsedate
from functools import partial
from pathlib import Path
from typing import Optional

# QGIS
from qgis.PyQt.QtCore import QCoreApplication, QUrl
from qgis.PyQt.QtGui import QDesktopServices

# project
from oslandia.__about__ import __title__, __version__
from oslandia.newsfeed.mdl_rss_item import RssItem
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.file_stats import is_file_older_than
from oslandia.toolbelt.network_manager import NetworkRequestsManager

# ############################################################################
# ########## Globals ###############
# ##################################

logger = logging.getLogger(__name__)

# ############################################################################
# ########## Classes ###############
# ##################################


class RssReader:
    """Minimalist RSS feed parser."""

    FEED_ITEMS: Optional[list[RssItem]] = None
    HEADERS: dict = {
        b"Accept": b"application/xml",
        b"User-Agent": bytes(f"{__title__}/{__version__}", "utf8"),
    }
    XML_NAMESPACES: dict[str, str] = {
        "atom": "http://www.w3.org/2005/Atom",
        "content": "http://purl.org/rss/1.0/modules/content/",
        "dc": "http://purl.org/dc/elements/1.1/",
        "slash": "http://purl.org/rss/1.0/modules/slash/",
        "sy": "http://purl.org/rss/1.0/modules/syndication/",
        "wfw": "http://wellformedweb.org/CommentAPI/",
    }

    def __init__(self):
        """Class initialization."""
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager.get_plg_settings()
        self.ntwk_manager = NetworkRequestsManager()

        self.local_feed_filepath: Path = self.plg_settings.local_app_folder.joinpath(
            "rss.xml"
        )

    def process(self):
        """Download, parse and read RSS feed than store items as attribute."""
        # download remote RSS feed to cache folder
        self.download_feed()
        if not self.local_feed_filepath.exists():
            self.log(
                message=self.tr(
                    "The RSS feed is not available locally. "
                    "Disabling RSS reader related features."
                ),
                log_level=1,
            )
            return

        # parse the local RSS feed
        self.read_feed()

        # check if a new item has been published since last check
        if not self.has_new_content:
            self.log(message="No new item found in RSS feed.", log_level=4)
            return
        # notify
        if isinstance(self.latest_item, RssItem):
            latest_item = self.latest_item
            self.log(
                message="{} {}".format(
                    self.tr("New content published:"),
                    latest_item.title,
                ),
                log_level=3,
                push=PlgOptionsManager().get_plg_settings().notify_push_info,
                duration=PlgOptionsManager().get_plg_settings().notify_push_duration,
                button=True,
                button_text=self.tr("Read it!"),
                button_connect=partial(self.on_read_item, latest_item),
            )

    def download_feed(self) -> bool:
        """Download RSS feed locally if it's older than latest 24 hours.

        :return: True is a new file has been downloaded.
        :rtype: bool
        """
        if is_file_older_than(
            local_file_path=self.local_feed_filepath,
            expiration_rotating_hours=self.plg_settings.rss_poll_frequency_hours,
        ):
            self.ntwk_manager.download_file_to(
                remote_url=self.plg_settings.rss_source,
                local_path=self.local_feed_filepath,
            )
            self.log(
                message=f"The remote RSS feed ({self.plg_settings.rss_source}) has been "
                f"downloaded to {self.local_feed_filepath}",
                log_level=0,
            )
            return True
        self.log(
            message=f"A fresh local RSS feed already exists: {self.local_feed_filepath}",
            log_level=0,
        )
        return False

    def on_read_item(self, rss_item: RssItem):
        """Slot ran when end-user want to a read an item.

        :param rss_item: RSS item.
        :type rss_item: RssItem
        """
        QDesktopServices.openUrl(QUrl(rss_item.url))

        self.plg_settings.latest_content_guid = rss_item.guid
        PlgOptionsManager.save_from_object(self.plg_settings)

    def read_feed(self) -> list[RssItem]:
        """Parse the feed XML as string and store items into an ordered list of RSS items.

        :return: list of RSS items dataclasses
        :rtype: list[RssItem]
        """
        feed_items: list[RssItem] = []
        tree = ET.parse(self.local_feed_filepath)
        items = tree.findall(path="channel/item", namespaces=self.XML_NAMESPACES)
        for item in items:
            try:
                # parse item and load it as object
                feed_item_obj = RssItem(
                    abstract=item.find("description").text,
                    authors=[
                        author.text
                        for author in item.findall(
                            "creator", namespaces=self.XML_NAMESPACES
                        )
                    ]
                    or None,
                    categories=[category.text for category in item.findall("category")]
                    or None,
                    date_pub=parsedate(item.find("pubDate").text),
                    guid=item.find("guid").text,
                    title=item.find("title").text,
                    url=item.find("link").text,
                )
                if item.find("enclosure") is not None:
                    item_enclosure = item.find("enclosure")
                    feed_item_obj.image_length = (item_enclosure.attrib.get("length"),)
                    feed_item_obj.image_type = (item_enclosure.attrib.get("type"),)
                    feed_item_obj.image_url = (item_enclosure.attrib.get("url"),)

                # add items to the feed
                feed_items.append(feed_item_obj)
            except Exception as err:
                item_idx: Optional[int] = None
                if hasattr(items, "index"):
                    item_idx = items.index(item)

                err_msg = f"Feed item {item_idx} triggers an error. Trace: {err}"
                logger.error(err_msg, stack_info=True)
                self.log(message=err_msg, log_level=2)

        # store feed items as attribute and return it
        self.FEED_ITEMS = feed_items
        return feed_items

    @property
    def latest_item(self) -> Optional[RssItem]:
        """Returns the latest feed item, based on index 0.

        :return: latest feed item.
        :rtype: RssItem
        """
        if not self.FEED_ITEMS:
            logger.warning(
                "Feed has not been loaded, so it's impossible to "
                "return the latest item."
            )
            return None

        return self.FEED_ITEMS[0]

    def latest_items(self, count: int = 10) -> list[RssItem]:
        """Returns the latest feed items.
        :param count: number of items to fetch
        :type count: int

        :return: latest feed items
        :rtype: List[RssItem]
        """
        if count <= 0:
            raise ValueError("Number of RSS items to get must be > 0")
        if not self.FEED_ITEMS:
            logger.warning(
                "Feed has not been loaded, so it's impossible to "
                "return the latest item."
            )
            return []
        return self.FEED_ITEMS[:count]

    @property
    def has_new_content(self) -> bool:
        """Compare the saved item guid (in plugin settings) with feed latest item to \
        determine if a newer item has been published.

        :return: True is a newer item has been published.
        :rtype: bool
        """
        settings = PlgOptionsManager.get_plg_settings()
        if (
            isinstance(self.latest_item, RssItem)
            and self.latest_item.guid != settings.latest_content_guid
        ):
            return True
        else:
            return False

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)
