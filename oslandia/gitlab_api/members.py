import json
from dataclasses import dataclass

# PyQGIS
from qgis.PyQt.QtCore import QUrl

# plugin
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Members:
    id: str
    name: str
    avatar_url: str


class MembersRequestManager:

    MAX_LIMIT = 50

    def get_base_url(self, project: str) -> str:
        """
        Get base url for members of a group

        Args:
            project: (str) project id

        Returns: url for members info

        """
        return f"{self.plg_settings.gitlab_api_url_base}/projects/{project}/members"

    def __init__(self):
        """
        Helper for stored_data request

        """
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_members_list(self, project: str) -> list[Members]:
        """
        Get list of members

        Args:
            group: (str) group id

        Returns: list of available members, raise ReadStoredDataException otherwise
        """
        self.log(f"{__name__}.get_members_list(project:{project})")

        i = 1
        result = []
        result_page = self._get_members_list(project, i, self.MAX_LIMIT)
        while len(result_page) != 0:
            result += result_page
            i = i + 1
            result_page = self._get_members_list(project, i, self.MAX_LIMIT)

        return result

    def _get_members_list(
        self, project_id: str, page: int = 1, limit: int = MAX_LIMIT
    ) -> list[Members]:
        """
        Get list of members

        Args:
            group: (str) group id
            page: (int) page number (start at 1)
            limit: (int)

        Returns: list of available members
        """

        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(
                f"{self.plg_settings.gitlab_api_url_base}/projects/{project_id}/members/all?page={page}&per_page={limit}"
            ),
            self.plg_settings.authentification_config_id,
        )
        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))
            return [
                Members(
                    name=p_data["name"],
                    avatar_url=p_data["avatar_url"],
                    id=p_data["id"],
                )
                for p_data in data
            ]
        return []
