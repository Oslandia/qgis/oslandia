import json
from dataclasses import dataclass
from typing import Optional

# PyQGIS
from qgis.PyQt.QtCore import QUrl

# plugin
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Group:
    id: str
    full_path: str
    name: str
    avatar_url: str


class GroupRequestManager:

    MAX_LIMIT = 50

    def get_base_url(self) -> str:
        """
        Get base url for group

        Returns: url for group info

        """
        return f"{self.plg_settings.gitlab_api_url_base}groups"

    def __init__(self):
        """
        Helper for stored_data request

        """
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_group_from_name(
        self, name: str, full_path: Optional[str] = None, group_id: Optional[str] = None
    ) -> Optional[Group]:
        """
        Get a group from name, None if not found

        Args:
            name(str) : group name
            full_path: group full path to search for. Defaults to None.
            group_id: group id to search for. Defaults to None.

        Returns:
            found Group, None if not found
        """
        groups = self.get_group_list(search=name)

        # filters
        if full_path:
            groups = [g for g in groups if g.full_path == full_path]
        if group_id:
            groups = [g for g in groups if g.id == group_id]

        return groups[0] if groups else None

    def get_group_from_id(self, group_id: str) -> Optional[Group]:
        """
        Get a group from name, None if not found

        Args:
            group_id(str) : group id

        Returns:
            found Group, None if not found
        """
        url = f"{self.get_base_url()}/{group_id}"
        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(url),
            self.plg_settings.authentification_config_id,
        )

        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))
            return Group(
                id=data["id"],
                full_path=data["full_path"],
                name=data["name"],
                avatar_url=data["avatar_url"],
            )

    def get_group_list(self, search: str = "") -> list[Group]:
        """
        Get list of group

        Args:
            search: (str): search param (defaut "")

        Returns: list of available group
        """
        self.log(f"{__name__}.get_group_list()")

        i = 1
        result = []
        result_page = self._get_group_list(search=search, page=i, limit=self.MAX_LIMIT)
        while len(result_page) != 0:
            result += result_page
            i = i + 1
            result_page = self._get_group_list(
                search=search, page=i, limit=self.MAX_LIMIT
            )
        return result

    def _get_group_list(
        self, search: str = "", page: int = 1, limit: int = MAX_LIMIT
    ) -> list[Group]:
        """
        Get list of group

        Args:
            search: (str): search param (defaut "")
            page: (int) page number (start at 1)
            limit: (int)

        Returns: list of available project
        """
        url = f"{self.get_base_url()}?page={page}&per_page={limit}"
        if search != "":
            url += f"&search={search}"

        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(url),
            self.plg_settings.authentification_config_id,
        )
        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))

            return [
                Group(
                    id=p_data["id"],
                    full_path=p_data["full_path"],
                    name=p_data["name"],
                    avatar_url=p_data["avatar_url"],
                )
                for p_data in data
            ]
        return []
