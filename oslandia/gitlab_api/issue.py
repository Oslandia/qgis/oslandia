import json
from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, Tuple

# PyQGIS
from qgis.PyQt.QtCore import QByteArray, QUrl

# plugin
from oslandia.gitlab_api.discussions import Discussion, DiscussionsRequestManager
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Issues:
    id: str
    iid: str
    project_id: str
    name: str
    created_date: datetime
    state: str
    description: str
    web_url: str
    iid: str
    labels: list[str] = field(default_factory=lambda: [])
    assignee_id: Optional[int] = None

    def valid_for_creation(self) -> bool:
        """Check if issue is valid for creation

        :return: True if issue is valid, False otherwise
        :rtype: bool
        """
        valid = self.name.strip() != ""
        return valid


class IssuesRequestManager:

    MAX_LIMIT = 50

    def get_base_url(self, project_id: str) -> str:
        """
        Get base url for members of a group

        Args:
            project: (str) project id

        Returns: url for members info

        """
        return f"{self.plg_settings.gitlab_api_url_base}projects/{project_id}/issues"

    def __init__(self):
        """
        Helper for stored_data request

        """
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_issues_list(self, project: str) -> list[Issues]:
        """
        Get list of members

        Args:
            project: (str) project id

        Returns: list of available members, raise ReadStoredDataException otherwise
        """
        self.log(f"{__name__}.get_issue_list(project:{project})")

        result = []
        result_page, _, nb_page = self.get_issues_list_by_page(
            project_id=project, page=1, limit=self.MAX_LIMIT
        )
        result += result_page
        for page in range(1, nb_page):
            result_page, _, _ = self.get_issues_list_by_page(
                project, page, self.MAX_LIMIT
            )
            result += result_page

        return result

    def get_issues_list_by_page(
        self, project_id: str, page: int = 1, limit: int = MAX_LIMIT
    ) -> Tuple[list[Issues], int, int]:
        """
        Get list of issues

        Args:
            project: (str) project id
            page: (int) page number (start at 1)
            limit: (int)

        Returns: list of available members
        """

        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(
                f"{self.plg_settings.gitlab_api_url_base}/projects/{project_id}/issues?page={page}&per_page={limit}"
            ),
            self.plg_settings.authentification_config_id,
            return_req_reply=True,
        )
        if req_reply:
            if req_reply.hasRawHeader("X-Total".encode()):
                nb_result = int(
                    req_reply.rawHeader("X-Total".encode()).data().decode("utf-8")
                )
            else:
                nb_result = -1
            if req_reply.hasRawHeader("X-Total-Pages".encode()):
                nb_page = int(
                    req_reply.rawHeader("X-Total-Pages".encode()).data().decode("utf-8")
                )
            else:
                nb_page = -1

            data = json.loads(req_reply.content().data().decode("utf-8"))
            return (
                [self._issue_from_data(p_data) for p_data in data],
                nb_result,
                nb_page,
            )
        return [], 0, 0

    def _issue_from_data(self, data: dict) -> Issues:
        """Create an issue from data dict

        :param data: input data dict
        :type data: dict
        :return: issue from data
        :rtype: Issues
        """
        return Issues(
            id=data["id"],
            iid=data["iid"],
            project_id=data["project_id"],
            name=data["title"],
            created_date=data["created_at"].strip(),
            state=data["state"],
            description=data["description"],
            labels=data["labels"],
            assignee_id=(data["assignee"]["id"] if data["assignee"] else None),
            web_url=data["web_url"],
        )

    def create_issue(self, project_id: str, issue: Issues) -> Issues:
        """
        Create an issue in a project

        Args:
            project_id: (str) project id
            issue: (Issues) issue to create
        """
        self.log(f"{__name__}.create_issue()")

        # encode data
        data = QByteArray()
        data_map = {
            "title": issue.name,
            "description": issue.description,
            "labels": issue.labels,
        }
        if issue.assignee_id:
            data_map["assignee_id"] = issue.assignee_id

        data.append(json.dumps(data_map))

        req_reply = self.ntwk_requester_blk.post_url(
            url=QUrl(self.get_base_url(project_id=project_id)),
            config_id=self.plg_settings.authentification_config_id,
            data=data,
            content_type_header="application/json",
        )
        data = json.loads(req_reply.data().decode("utf-8"))
        return self._issue_from_data(data)

    def create_issue_note(
        self, project_id: str, issue_iid: str, body: str
    ) -> Discussion:
        """
        Create an issue note

        Args:
            project_id: (str) project id
            issue_iid: (str) issue internal id
            body: (str) note body
        """
        self.log(f"{__name__}.create_issue_note()")

        # encode data
        data = QByteArray()
        data_map = {"body": body}
        data.append(json.dumps(data_map))

        req_reply = self.ntwk_requester_blk.post_url(
            url=QUrl(
                f"{self.get_base_url(project_id=project_id)}/{issue_iid}/discussions"
            ),
            config_id=self.plg_settings.authentification_config_id,
            data=data,
            content_type_header="application/json",
        )
        note_data = json.loads(req_reply.data().decode("utf-8"))
        return DiscussionsRequestManager.discussion_from_data(
            note_data, project_id, issue_iid
        )

    def update_issue(self, issue: Issues) -> None:
        """
        Update an issue

        Args:
            issue: (Issues) : issue to update
        """
        self.log(f"{__name__}.update_issue()")

        # encode data
        data = QByteArray()
        data_map = {
            "id": issue.id,
            "assignee_ids": [issue.assignee_id],
            "description": issue.description,
            "title": issue.name,
            "labels": issue.labels,
        }
        data.append(json.dumps(data_map))

        self.ntwk_requester_blk.put_url(
            url=QUrl(f"{self.get_base_url(project_id=issue.project_id)}/{issue.iid}"),
            config_id=self.plg_settings.authentification_config_id,
            data=data,
            content_type_header="application/json",
        )

    def update_issue_note(
        self, project_id: str, issue_iid: str, note_id: str, body: str
    ) -> None:
        """
        Update an issue note

        Args:
            project_id: (str) project id
            issue_iid: (str) issue internal id
            note_id: (str) note id
            body: (str) note body
        """
        self.log(f"{__name__}.update_issue_note()")

        # encode data
        data = QByteArray()
        data_map = {"body": body}
        data.append(json.dumps(data_map))

        self.ntwk_requester_blk.put_url(
            url=QUrl(
                f"{self.get_base_url(project_id=project_id)}/{issue_iid}/notes/{note_id}"
            ),
            config_id=self.plg_settings.authentification_config_id,
            data=data,
            content_type_header="application/json",
        )
