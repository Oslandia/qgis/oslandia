# standard
import json
from dataclasses import dataclass, field

# PyQGIS
from qgis.PyQt.QtCore import QByteArray, QUrl

# plugin
from oslandia.gitlab_api.user import User
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Note:
    id: str
    body: str
    system: bool
    author: User
    created_at: str
    updated_at: str

    project_id: str
    issue_iid: str


@dataclass
class Discussion:
    id: str
    project_id: str
    issue_iid: str
    notes: list[Note] = field(default_factory=lambda: [])


class DiscussionsRequestManager:

    MAX_LIMIT = 50

    def get_base_url(self, project_id: str, issue_iid: str) -> str:
        """
        Get base url for discussion of an issue

        Args:
            project_id: (str) project id
            issue_iid: (str) issue id

        Returns: url for discussion info

        """
        return f"{self.plg_settings.gitlab_api_url_base}projects/{project_id}/issues/{issue_iid}/discussions"

    def __init__(self):
        """
        Helper for discussions request

        """
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_discussions_list(self, project_id: str, issue_iid: str) -> list[Discussion]:
        """
        Get list of discussion

        Args:
            project: (str) project id
            issue_iid: (str) issue id

        Returns: list of available discussion
        """
        self.log(
            f"{__name__}.get_discussion_list(project:{project_id},project:{project_id})"
        )

        i = 1
        result = []
        result_page = self._get_discussions_list(
            project_id, issue_iid, i, self.MAX_LIMIT
        )
        while len(result_page) != 0:
            result += result_page
            i = i + 1
            result_page = self._get_discussions_list(
                project_id, issue_iid, i, self.MAX_LIMIT
            )

        return result

    @staticmethod
    def note_from_data(data: dict, project_id: str, issue_iid: str) -> Note:
        """Create a note from a data dict

        :param data: input data dict
        :type data: dict
        :param project_id: project id
        :type project_id: str
        :param issue_iid: issue internal id
        :type issue_iid: str
        :return: note from data
        :rtype: Note
        """
        return Note(
            id=data["id"],
            body=data["body"],
            system=data["system"],
            created_at=data["created_at"],
            updated_at=data["updated_at"],
            author=User(
                id=data["author"]["id"],
                name=data["author"]["name"],
                username=data["author"]["username"],
                avatar_url=data.get("author", {}).get("avatar_url"),
            ),
            project_id=project_id,
            issue_iid=issue_iid,
        )

    @staticmethod
    def discussion_from_data(data: dict, project_id: str, issue_iid: str) -> Discussion:
        """Create a discussion from a data dict

        :param data: input data dict
        :type data: dict
        :param project_id: project id
        :type project_id: str
        :param issue_iid: issue internal id
        :type issue_iid: str
        :return: discussion from data
        :rtype: Discussion
        """
        return Discussion(
            id=data["id"],
            project_id=project_id,
            issue_iid=issue_iid,
            notes=[
                DiscussionsRequestManager.note_from_data(n_data, project_id, issue_iid)
                for n_data in data["notes"]
            ],
        )

    def _get_discussions_list(
        self, project_id: str, issue_iid: str, page: int = 1, limit: int = MAX_LIMIT
    ) -> list[Discussion]:
        """
        Get list of discussions

        Args:
            project_id: (str) project id
            issue_iid: (str) issue iid
            page: (int) page number (start at 1)
            limit: (int)

        Returns: list of available discussions
        """

        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(
                f"{self.get_base_url(project_id,issue_iid)}?page={page}&per_page={limit}"
            ),
            self.plg_settings.authentification_config_id,
        )
        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))
            return [
                self.discussion_from_data(p_data, project_id, issue_iid)
                for p_data in data
            ]
        return []

    def add_note_reply(
        self, project_id: str, issue_iid: str, discussion_id: str, body: str
    ) -> Note:
        """
        Add a note reply to a discussion

        Args:
            project_id: (str) project id
            issue_iid: (str) issue internal id
            discussion_id: (str) discussion id
            body: (str) note reply body
        """
        self.log(f"{__name__}.add_note_reply()")

        # encode data
        data = QByteArray()
        data_map = {"body": body}
        data.append(json.dumps(data_map))

        req_reply = self.ntwk_requester_blk.post_url(
            url=QUrl(
                f"{self.get_base_url(project_id=project_id,issue_iid=issue_iid)}/{discussion_id}/notes"
            ),
            config_id=self.plg_settings.authentification_config_id,
            data=data,
            content_type_header="application/json",
        )
        note_data = json.loads(req_reply.data().decode("utf-8"))
        return self.note_from_data(note_data, project_id, issue_iid)
