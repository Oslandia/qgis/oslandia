import json
from dataclasses import dataclass

# PyQGIS
from qgis.PyQt.QtCore import QUrl

# plugin
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Labels:
    id: str
    name: str
    description: str


class LabelsRequestManager:

    MAX_LIMIT = 50

    def get_base_url(self, project: str) -> str:
        """
        Get base url for labels of a group

        Args:
            project: (str) project id

        Returns: url for labels info

        """
        return f"{self.plg_settings.gitlab_api_url_base}/projects/{project}/labels"

    def __init__(self):
        """
        Helper for stored_data request

        """
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_labels_list(self, project_id: str) -> list[Labels]:
        """
        Get list of labels

        Args:
            project: (str) group id

        Returns: list of available labels, raise ReadStoredDataException otherwise
        """
        self.log(f"{__name__}.get_labels_list(project:{project_id})")
        i = 1
        result = []
        result_page = self._get_labels_list(project_id, i, self.MAX_LIMIT)
        while len(result_page) != 0:
            result += result_page
            i = i + 1
            result_page = self._get_labels_list(project_id, i, self.MAX_LIMIT)
        return result

    def _get_labels_list(
        self, project_id: str, page: int = 1, limit: int = MAX_LIMIT
    ) -> list[Labels]:
        """
        Get list of labels

        Args:
            group: (str) group id
            page: (int) page number (start at 1)
            limit: (int)

        Returns: list of available labels
        """

        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(
                f"{self.plg_settings.gitlab_api_url_base}projects/{project_id}/labels?page={page}&per_page={limit}"
            ),
            self.plg_settings.authentification_config_id,
        )
        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))
            return [
                Labels(
                    id=p_data["id"],
                    name=p_data["name"],
                    description=p_data["description"],
                )
                for p_data in data
            ]
        return []
