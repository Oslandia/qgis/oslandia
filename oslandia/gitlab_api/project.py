import json
from dataclasses import dataclass, field
from datetime import datetime
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt.QtCore import QUrl

# plugin
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


@dataclass
class Project:
    id: str
    name: str
    group: str
    archived: bool
    created_at: datetime
    avatar_url: Optional[str] = None
    # defined during post init
    api_url: str = field(init=False)
    avatar_local_filepath: Path = field(init=False)

    def __post_init__(self):
        """Post init method to define some of properties of the class."""
        plg_settings = PlgOptionsManager.get_plg_settings()
        local_avatars_cache_folder: Path = plg_settings.local_app_folder.joinpath(
            "avatars"
        )
        self.avatar_local_filepath = local_avatars_cache_folder.joinpath(
            f"prj_{self.id}.png"
        )
        self.api_url = f"{plg_settings.gitlab_api_url_base}projects/{self.id}/"


class ProjectRequestManager:

    MAX_LIMIT: int = 50

    def __init__(self):
        """Helper for stored_data request."""
        self.log = PlgLogger().log
        self.ntwk_requester_blk = NetworkRequestsManager()
        self.plg_settings = PlgOptionsManager.get_plg_settings()

    def get_base_url(self, group_id: str) -> str:
        """Get base url for listing projects of a group.

        :param group_id: group unique identifier
        :type group_id: str

        :return: URL for group's projects informations
        :rtype: str
        """
        return f"{self.plg_settings.gitlab_api_url_base}groups/{group_id}/projects"

    def get_project_list(self, group_id: str) -> list[Project]:
        """Get list of group's project.

        :param group_id: group unique identifier
        :type group_id: str

        :raises ReadStoredDataException: otherwise

        :return: list of available project
        :rtype: list[Project]
        """
        self.log(f"{__name__}.get_project_list(group:{group_id})")

        i = 1
        result = []
        result_page = self._get_project_list(group_id, i, self.MAX_LIMIT)
        while len(result_page) != 0:
            result += result_page
            i = i + 1
            result_page = self._get_project_list(group_id, i, self.MAX_LIMIT)
        return result

    def _get_project_list(
        self, group_id: str, page: int = 1, limit: int = MAX_LIMIT
    ) -> list[Project]:
        """Get list of projects for a group..

        :param group_id: group unique identifier
        :type group_id: str
        :param page: page number, defaults to 1
        :type page: int, optional
        :param limit: number of results per page, defaults to MAX_LIMIT
        :type limit: int, optional

        :return: list of available project
        :rtype: list[Project]
        """
        # send request
        req_reply = self.ntwk_requester_blk.get_url(
            QUrl(f"{self.get_base_url(group_id)}?page={page}&per_page={limit}"),
            self.plg_settings.authentification_config_id,
        )
        if req_reply:
            data = json.loads(req_reply.data().decode("utf-8"))

            return [
                Project(
                    id=p_data["id"],
                    name=p_data["name"],
                    group=p_data["namespace"]["name"],
                    archived=p_data["archived"],
                    created_at=datetime.fromisoformat(p_data["created_at"].strip()),
                    avatar_url=p_data.get("avatar_url"),
                )
                for p_data in data
            ]
        return []

    def download_avatar(self, project: Project) -> Optional[Path]:
        """Download a project's avatar if it does not exist in the local cache folder.
        File is stored in local cache.

        :param project: project to download avatar for
        :type project: Project

        :return: local path to the downloaded avatar
        :rtype: Optional[Path]
        """
        if (
            not project.avatar_local_filepath.exists()
            and project.avatar_url is not None
        ):
            self.log(
                f"Downloading avatar for {project.name}",
                log_level=0,
                push=False,
            )
            self.ntwk_requester_blk.download_file_to(
                remote_url=QUrl(f"{project.api_url}avatar"),
                local_path=project.avatar_local_filepath,
                auth_cfg=self.plg_settings.authentification_config_id,
            )
        return project.avatar_local_filepath
