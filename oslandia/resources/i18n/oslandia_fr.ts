<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AuthenticationDialog</name>
    <message>
        <location filename="../../gui/dlg_authentication.ui" line="14"/>
        <source>Authentication</source>
        <translation>Authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_authentication.ui" line="26"/>
        <source>Connect</source>
        <translation>S&apos;authentifier</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_authentication.py" line="100"/>
        <source>Welcome {user.name}!</source>
        <translation>Bienvenue {user.name} !</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_authentication.py" line="108"/>
        <source>Invalid connection parameters: {exc}</source>
        <translation>Paramètres de connexion invalides : {exc}</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_authentication.ui" line="39"/>
        <source>To connect to Oslandia project, Please log in to the web browser that will open.</source>
        <translation>Pour accéder à votre offre de support, merci de vous authentifier et d&apos;autoriser l&apos;appairage dans le navigateur web qui va s&apos;ouvrir.</translation>
    </message>
</context>
<context>
    <name>AutoResizingTextEdit</name>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="29"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="32"/>
        <source>Ctrl+B</source>
        <comment>Bold</comment>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="35"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="38"/>
        <source>Ctrl+I</source>
        <comment>Italic</comment>
        <translation>Ctrl+i</translation>
    </message>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="41"/>
        <source>Strike</source>
        <translation>Barré</translation>
    </message>
    <message>
        <location filename="../../gui/auto_resizing_text_edit.py" line="60"/>
        <source>Insert code</source>
        <translation>Insérer un bloc de code</translation>
    </message>
</context>
<context>
    <name>CacheManager</name>
    <message>
        <location filename="../../toolbelt/cache_manager.py" line="112"/>
        <source>Can&apos;t open cache file: {}</source>
        <translation>Impossible d&apos;ouvrir le fichier de cache : {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/cache_manager.py" line="131"/>
        <source>Can&apos;t open cache file for write: {}</source>
        <translation>Impossible d&apos;ouvrir le fichier de cache en écriture : {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/cache_manager.py" line="166"/>
        <source>Cache dir {} has been removed.</source>
        <translation>Le dossier de cache {} a été supprimé.</translation>
    </message>
</context>
<context>
    <name>ConfigOptionsPage</name>
    <message>
        <location filename="../../gui/dlg_settings.py" line="182"/>
        <source>Read history has been reset.</source>
        <translation>L&apos;historique de lecture a été réinitialisé.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="216"/>
        <source>Welcome</source>
        <translation>Bienvenue</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="216"/>
        <source>Welcome {user.name}!</source>
        <translation>Bienvenue {user.name} !</translation>
    </message>
</context>
<context>
    <name>CreateIssueDialog</name>
    <message>
        <location filename="../../gui/dlg_create_issue.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_create_issue.ui" line="39"/>
        <source>Include archived</source>
        <translation>Inclure les tickets archivés</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_create_issue.py" line="36"/>
        <source>Create issue</source>
        <translation>Créer un ticket</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_create_issue.py" line="64"/>
        <source>Submit</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_create_issue.py" line="99"/>
        <source>Issue not valid.
Please fill mandatory parameters.</source>
        <translation>Ticket invalide.
Merci de remplir les champs requis.</translation>
    </message>
</context>
<context>
    <name>DiscussionWidget</name>
    <message>
        <location filename="../../gui/wdg_discussion.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_discussion.ui" line="32"/>
        <source>Reply</source>
        <translation>Réponse</translation>
    </message>
</context>
<context>
    <name>DisplayMode</name>
    <message>
        <location filename="../../gui/wdg_discussion.py" line="80"/>
        <source>{} reply Last reply by {} - {}</source>
        <translation>{} réponse. Dernière réponse par {} - {}</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.py" line="68"/>
        <source>Description *</source>
        <translation>Description*</translation>
    </message>
</context>
<context>
    <name>GitLabTextEdit</name>
    <message>
        <location filename="../../gui/gitlab_text_edit.py" line="24"/>
        <source>Attach file</source>
        <translation>Joindre un fichier</translation>
    </message>
    <message>
        <location filename="../../gui/gitlab_text_edit.py" line="28"/>
        <source>Attach file or image.</source>
        <translation>Joindre un fichier ou une image.</translation>
    </message>
    <message>
        <location filename="../../gui/gitlab_text_edit.py" line="34"/>
        <source>Select file to upload</source>
        <translation>Sélectionner le fichier à insérer</translation>
    </message>
</context>
<context>
    <name>IssueContextAuthorisationWidget</name>
    <message>
        <location filename="../../gui/wdg_issue_context_authorisation.ui" line="14"/>
        <source>Authoring information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue_context_authorisation.ui" line="35"/>
        <source>Issue context information</source>
        <translation>Contexte du ticket</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue_context_authorisation.ui" line="47"/>
        <source>Send QGIS informations</source>
        <translation>Joindre les informations sur QGIS (équivalent du menu À propos)</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue_context_authorisation.ui" line="54"/>
        <source>Send active plugins informations</source>
        <translation>Joindre la liste des plugins actifs</translation>
    </message>
</context>
<context>
    <name>IssueWidget</name>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="134"/>
        <source>Labels</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="61"/>
        <source>Title *</source>
        <translation>Titre*</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="79"/>
        <source>The title should start with the issue type: [BUG], [QUESTION], [FEATURE], etc.</source>
        <translation>Un bon titre commence avec le type de rapport : [BUG], [QUESTION], [EVOLUTION], etc.</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="48"/>
        <source>Assignee</source>
        <translation>Responsable</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="177"/>
        <source>Comments</source>
        <translation>Fil de discussion</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="112"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Detail the purpose of this issue. The &lt;a href=&quot;https://docs.gitlab.com/ee/user/markdown.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Markdown syntax (GitLab flavored)&lt;/span&gt;&lt;/a&gt; is supported to help you to structure your description.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Détailler votre demande. La &lt;a href=&quot;https://docs.gitlab.com/ee/user/markdown.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;syntaxe Markdown (GitLab flavored)&lt;/span&gt;&lt;/a&gt; est supportée pour vous aider à structurer et mettre en forme votre description.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="143"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="150"/>
        <source>Save</source>
        <translation>Publier</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_issue.ui" line="157"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>IssuesListModel</name>
    <message>
        <location filename="../../gui/mdl_issue.py" line="26"/>
        <source>Created</source>
        <translation>Créé le</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_issue.py" line="26"/>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_issue.py" line="26"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_issue.py" line="26"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
</context>
<context>
    <name>LabelsListModel</name>
    <message>
        <location filename="../../gui/mdl_labels.py" line="21"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_labels.py" line="21"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
</context>
<context>
    <name>MarkdownPreviewWidget</name>
    <message>
        <location filename="../../gui/wdg_markdown_preview.ui" line="66"/>
        <source>Preview</source>
        <translation>Rendu</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_markdown_preview.ui" line="56"/>
        <source>Enable auto generation of preview every time the input text changes.</source>
        <translation>Activer le rendu automatique au fur et à mesure que le texte change.</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_markdown_preview.ui" line="59"/>
        <source>Auto-preview</source>
        <translation>Rendu automatique</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_markdown_preview.ui" line="116"/>
        <source>Enter text in body editing box and click on preview button to see your Markdown text rendered here.</source>
        <translation>Rédiger ici en Markdown et cliquer sur &quot;Rendu&quot; pour avoir une idée du résultat final.</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_markdown_preview.ui" line="14"/>
        <source>Markdown preview</source>
        <translation>Éditeur Markdown - Rendu</translation>
    </message>
</context>
<context>
    <name>MembersListModel</name>
    <message>
        <location filename="../../gui/mdl_members.py" line="25"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>NetworkRequestsManager</name>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="314"/>
        <source>Houston, we&apos;ve got a problem: {}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NoteCreationWidget</name>
    <message>
        <location filename="../../gui/wdg_note_creation.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note_creation.ui" line="20"/>
        <source>Comment</source>
        <translation>Ajouter un commentaire</translation>
    </message>
</context>
<context>
    <name>NoteReplyCreationWidget</name>
    <message>
        <location filename="../../gui/wdg_note_reply_creation.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note_reply_creation.ui" line="20"/>
        <source>Reply</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NoteWidget</name>
    <message>
        <location filename="../../gui/wdg_note.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="56"/>
        <source>&lt;user_name&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="43"/>
        <source>&lt;name&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="80"/>
        <source>&lt;date&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="63"/>
        <source>Save</source>
        <translation>Publier</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="73"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_note.ui" line="87"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>OslandiaPlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="108"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="117"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="126"/>
        <source>Create issue</source>
        <translation>Créer un ticket</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="134"/>
        <source>View issue</source>
        <translation>Consulter le ticket</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="140"/>
        <source>Plugins by Oslandia</source>
        <translation>Plugins et outils liés à QGIS par Oslandia</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="148"/>
        <source>Connect</source>
        <translation>S&apos;authentifier</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="156"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="213"/>
        <source>Authentication configuration ID is not set in settings. Please use the authenticatio dialog to pair your GitLab account.</source>
        <translation>Aucune configuration d&apos;authentification associée dans les paramètres. Merci de relancer l&apos;authentification pour apparier le plugin avec votre compte GitLab.</translation>
    </message>
</context>
<context>
    <name>OslandiaSettingsForm</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="36"/>
        <source>News Feed</source>
        <translation>Flux RSS</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="55"/>
        <source>Erase feed clicked items history:</source>
        <translation>Effacer l&apos;historique de lecture :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="62"/>
        <source>Check feed every:</source>
        <translation>Fréquence de mise à jour :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="69"/>
        <source>Notification duration:</source>
        <translation>Durée de la notification :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="95"/>
        <source> hours</source>
        <translation> heures</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="111"/>
        <source>0 means no duration, notification will be displayed until end-user click on close.</source>
        <translation>0 signifie la notification est persistante jusqu&apos;à ce que l&apos;utilisateur final ne la ferme.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="126"/>
        <source> seconds</source>
        <translation> secondes</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="151"/>
        <source>Feed URL:</source>
        <translation>URL du flux RSS :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="164"/>
        <source>Display a push message when a new content is published</source>
        <translation>Afficher une notification quand un nouveau contenu est publié</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="171"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="470"/>
        <source>PluginTitle - Version X.X.X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="288"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="312"/>
        <source>Version used to save settings:</source>
        <translation>Paramètres sauvegardés avec la version :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="439"/>
        <source>Reset to factory default</source>
        <translation>Remettre les réglages par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="408"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="417"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode debug (performances potentiellement dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="334"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="376"/>
        <source>Plugin Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="230"/>
        <source>Support End-Customers</source>
        <translation>Clients de l&apos;offre support Oslandia</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="266"/>
        <source>Check authorization</source>
        <translation>Vérifier l&apos;authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="236"/>
        <source>How to authorize the plugin?</source>
        <translation>Comment s&apos;authentifier ?</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="389"/>
        <source>Clear cache</source>
        <translation>Vider le cache</translation>
    </message>
</context>
<context>
    <name>PluginsBrowser</name>
    <message>
        <location filename="../../gui/dlg_plugins_browser.py" line="63"/>
        <source>Information last updated: </source>
        <translation>Dernière mise à jour : </translation>
    </message>
</context>
<context>
    <name>ProjectListModel</name>
    <message>
        <location filename="../../gui/mdl_project.py" line="26"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_project.py" line="26"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_project.py" line="26"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/mdl_project.py" line="26"/>
        <source>Archived</source>
        <translation>Archivé</translation>
    </message>
</context>
<context>
    <name>RssReader</name>
    <message>
        <location filename="../../newsfeed/rss_reader.py" line="74"/>
        <source>The RSS feed is not available locally. Disabling RSS reader related features.</source>
        <translation>Le flux RSS n&apos;est pas disponible. Les fonctionnalités liées sont désactivées.</translation>
    </message>
    <message>
        <location filename="../../newsfeed/rss_reader.py" line="93"/>
        <source>New content published:</source>
        <translation>Nouvel article publié :</translation>
    </message>
    <message>
        <location filename="../../newsfeed/rss_reader.py" line="93"/>
        <source>Read it!</source>
        <translation>Lire...</translation>
    </message>
</context>
<context>
    <name>TextEditPreviewWidget</name>
    <message>
        <location filename="../../gui/wdg_text_edit.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="95"/>
        <source>Preview</source>
        <translation>Rendu</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="95"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="49"/>
        <source>Bold</source>
        <translation type="obsolete">Gras</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="54"/>
        <source>Italic</source>
        <translation type="obsolete">Italique</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="59"/>
        <source>Strike</source>
        <translation type="obsolete">Barré</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="74"/>
        <source>Insert code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="81"/>
        <source>Attach file</source>
        <translation type="obsolete">Joindre un fichier</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="85"/>
        <source>Attach file or image.</source>
        <translation type="obsolete">Joindre un fichier ou une image.</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_text_edit.py" line="112"/>
        <source>Pick a file to insert</source>
        <translation>Sélectionner le fichier à insérer</translation>
    </message>
</context>
<context>
    <name>UserDialog</name>
    <message>
        <location filename="../../gui/dlg_user.ui" line="17"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_user.ui" line="46"/>
        <source>Log out</source>
        <translation>Se déconnecter</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_user.ui" line="59"/>
        <source>See on GitLab</source>
        <translation>Voir sur GitLab</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_user.py" line="78"/>
        <source>Oslandia - Connected as {}</source>
        <translation>Oslandia - Connecté en tant que {}</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_user.py" line="80"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_user.py" line="80"/>
        <source>Can&apos;t define current user : {exc}</source>
        <translation>Impossible de déteminer l&apos;utilisateur authentifié : {exc}</translation>
    </message>
</context>
<context>
    <name>UserWidget</name>
    <message>
        <location filename="../../gui/wdg_user.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_user.ui" line="47"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur :</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_user.ui" line="61"/>
        <source>Not the avatar you expected? Set the avatar you want by changing the image linked to your email address on https://gravatar.com/</source>
        <translation>Ceci n&apos;est pas votre avatar sur git.oslandia.net ? Normal, il n&apos;est pas accessible et nous utilisons l&apos;avatar lié à votre adresse email sur https://gravatar.com/</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_user.ui" line="118"/>
        <source>Email:</source>
        <translation>Email :</translation>
    </message>
    <message>
        <location filename="../../gui/wdg_user.ui" line="131"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
</context>
<context>
    <name>ViewIssueDialog</name>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="14"/>
        <source>Dialog</source>
        <translation>Navigateur de tickets</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="35"/>
        <source>Include archived</source>
        <translation>Inclure les tickets archivés</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="54"/>
        <source>Include closed</source>
        <translation>Inclure les tickets fermés</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.py" line="43"/>
        <source>Project issues</source>
        <translation>Tickets du projet</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="61"/>
        <source>Only assigned to me</source>
        <translation>Tickets qui me sont assignés</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="94"/>
        <source>New issue</source>
        <translation>Nouveau ticket</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="81"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.ui" line="106"/>
        <source>Quick filter</source>
        <translation>Filtre rapide</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.py" line="102"/>
        <source>Filter on issue title and description.</source>
        <translation>Filtrer sur la titre et la description.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.py" line="184"/>
        <source>Updated issue won&apos;t be saved. Do you want to change displayed issue ?</source>
        <translation>Le ticket édité ne va pas être publié. Voulez-vous éditer le ticket publié ?</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_view_issues.py" line="189"/>
        <source>Changes unsaved</source>
        <translation>Changements non publiés</translation>
    </message>
</context>
<context>
    <name>dlg_plugins_browser</name>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="14"/>
        <source>Plugins and QGIS tools by Oslandia</source>
        <translation>Plugins et outils liés à QGIS par Oslandia</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="108"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="411"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="352"/>
        <source>Issue to be funded</source>
        <translation>Idées et anomalies à sponsoriser</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="365"/>
        <source>Blog</source>
        <translation>Article</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="391"/>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="378"/>
        <source>Repository</source>
        <translation>Code source</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="238"/>
        <source>Tags:</source>
        <translation>Mots-clés :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="321"/>
        <source>Funders:</source>
        <translation>Sponsors :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="200"/>
        <source>About:</source>
        <translation>À propos :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="290"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="155"/>
        <source>Version:</source>
        <translation>Version :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_plugins_browser.ui" line="38"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
</TS>
