FORMS =	../../gui/dlg_authentication.ui \
	../../gui/dlg_create_issue.ui \
	../../gui/dlg_plugins_browser.ui \
	../../gui/dlg_settings.ui \
	../../gui/dlg_user.ui \
	../../gui/dlg_view_issues.ui \
	../../gui/wdg_discussion.ui \
	../../gui/wdg_issue.ui \
	../../gui/wdg_issue_context_authorisation.ui \
	../../gui/wdg_markdown_preview.ui \
	../../gui/wdg_note.ui \
	../../gui/wdg_note_creation.ui \
	../../gui/wdg_note_reply_creation.ui \
	../../gui/wdg_text_edit.ui \
	../../gui/wdg_user.ui

SOURCES =	../../gitlab_api/custom_exceptions.py \
	../../gitlab_api/discussions.py \
	../../gitlab_api/group.py \
	../../gitlab_api/issue.py \
	../../gitlab_api/labels.py \
	../../gitlab_api/members.py \
	../../gitlab_api/project.py \
	../../gitlab_api/upload.py \
	../../gitlab_api/user.py \
	../../gui/auto_resizing_text_browser.py \
	../../gui/auto_resizing_text_edit.py \
	../../gui/delegate_issue_table_view.py \
	../../gui/dlg_authentication.py \
	../../gui/dlg_create_issue.py \
	../../gui/dlg_plugins_browser.py \
	../../gui/dlg_settings.py \
	../../gui/dlg_user.py \
	../../gui/dlg_view_issues.py \
	../../gui/gitlab_text_browser.py \
	../../gui/gitlab_text_edit.py \
	../../gui/mdl_issue.py \
	../../gui/mdl_labels.py \
	../../gui/mdl_members.py \
	../../gui/mdl_project.py \
	../../gui/mdl_projects_browser.py \
	../../gui/proxy_model_issue.py \
	../../gui/proxy_model_project.py \
	../../gui/wdg_discussion.py \
	../../gui/wdg_issue.py \
	../../gui/wdg_issue_context_authorisation.py \
	../../gui/wdg_markdown_preview.py \
	../../gui/wdg_note.py \
	../../gui/wdg_note_creation.py \
	../../gui/wdg_note_reply_creation.py \
	../../gui/wdg_text_edit.py \
	../../gui/wdg_user.py \
	../../models/oauth2_configuration.py \
	../../newsfeed/mdl_rss_item.py \
	../../newsfeed/rss_reader.py \
	../../plugin_main.py \
	../../toolbelt/application_folder.py \
	../../toolbelt/cache_manager.py \
	../../toolbelt/file_stats.py \
	../../toolbelt/json_tools.py \
	../../toolbelt/log_handler.py \
	../../toolbelt/network_manager.py \
	../../toolbelt/plugin_manager.py \
	../../toolbelt/preferences.py \
	../../toolbelt/qgis_info.py \
	../../toolbelt/qt_utils.py

TRANSLATIONS =	oslandia_fr.ts
