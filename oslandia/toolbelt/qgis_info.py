# PyQGIS
from qgis import utils
from qgis.core import QgsApplication, QgsCommandLineUtils


def get_installed_qgis_info_as_markdown() -> str:
    """Get installed qgis information as markdown

    :return: installed qgis information as markdown
    :rtype: str
    """
    values = [val for val in QgsCommandLineUtils.allVersions().split("\n") if val]
    return "\n* " + "\n* ".join(values)


def get_active_plugin_info() -> dict[str, str]:
    """Get version of active plugin

    :return: dict of plugin name with version
    :rtype: Dict[str, str]
    """
    result = {}
    active_plugin_list = utils.active_plugins
    for plugin in active_plugin_list:
        result[plugin] = utils.pluginMetadata(plugin, "version")
    return result


def get_qgis_language() -> str:
    """Retrieve the current language of QGIS.

    This function returns the locale name of the QGIS application, which indicates the current language
    setting of the software.

    :return: The language of QGIS in 'language_COUNTRY' format (e.g., 'fr').
    :rtype: str
    """
    # Get the current language of QGIS
    language = QgsApplication.locale()
    return language
