#! python3  # noqa: E265

"""
Plugin settings.
"""


# standard
import os
from dataclasses import asdict, dataclass, fields
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication, QgsAuthMethodConfig, QgsSettings

# package
import oslandia.toolbelt.log_handler as log_hdlr
from oslandia.__about__ import DIR_PLUGIN_ROOT, __title__, __version__
from oslandia.datamodels import oauth2_configuration
from oslandia.toolbelt.application_folder import get_app_dir


# ############################################################################
# ########## Classes ###############
# ##################################
@dataclass
class Tenant:
    # name_tenant: str : il sera la clé du settings
    config: str


CFG_AUTH_NAME = "oslandia_plugin_cfg"


@dataclass
class PlgSettingsStructure:
    """Plugin settings structure and defaults values."""

    # global
    debug_mode: bool = False
    version: str = __version__
    local_app_folder: Path = get_app_dir(dir_name="cache")

    # RSS feed
    rss_source: str = "https://oslandia.com/feed/"
    rss_poll_frequency_hours: int = 24
    latest_content_guid: str = ""
    notify_push_info: bool = True
    notify_push_duration: int = 30

    # network and authentication
    authentification_config_id: Optional[str] = None
    gitlab_url_instance: str = "https://git.oslandia.net/"
    gitlab_api_version: str = "4"

    # GitLab group
    gitlab_group_name: str = "Assistance"
    gitlab_group_id: str = "519"
    gitlab_group_full_path: str = "support/assistance"

    # issue creation option
    allow_qgis_info_add_in_new_issue: bool = False
    allow_plugin_info_add_in_new_issue: bool = False

    # Projets browser (index 0 = cards / index 1 = list)
    project_browser_last_view: int = 0

    @property
    def gitlab_api_url_base(self) -> str:
        """Get GitLab instance API base URL.

        :return: https://{instance}/api/v{api_version}
        :rtype: str
        """
        return f"{self.gitlab_url_instance}api/v{self.gitlab_api_version}/"

    def create_auth_config(self) -> Optional[QgsAuthMethodConfig]:
        """Create QgsAuthMethodConfig for OAuth2 authentification.

        :return: created configuration. Warning: config must be added to QgsApplication.authManager() before use
        :rtype: Optional[QgsAuthMethodConfig]
        """
        newAU = QgsAuthMethodConfig(method="OAuth2", version=1)

        newAU.setId(QgsApplication.authManager().uniqueConfigId())
        newAU.setName(CFG_AUTH_NAME)

        json_config = Path(
            os.getenv(
                "PLUGIN_OSLANDIA_OAUTH2_CONFIG",
                DIR_PLUGIN_ROOT.joinpath("auth/oauth2_config.json"),
            )
        )

        data = oauth2_configuration.OAuth2Configuration.from_json(json_config)

        if not data:
            log_hdlr.PlgLogger.log(
                message="Error during authentification, please check environment variable",
                log_level=2,
                push=True,
            )
            return None

        compliant, missing_fields = data.is_json_compliant(json_config)

        if not compliant:
            log_hdlr.PlgLogger.log(
                message="The configuration file {} is not complete, the following fields are missing: {}".format(
                    json_config, missing_fields
                ),
                log_level=2,
                push=True,
            )

        # We need to use a string for config_map
        config_str = str(data.as_qgs_str_config_map())

        # ' not supported by pyqgis, replace by "
        config_str = config_str.replace("'", '"')

        # replace also boolean str
        config_str = config_str.replace("False", "false")
        config_str = config_str.replace("True", "true")

        config_map = {"oauth2config": config_str}
        newAU.setConfigMap(config_map)

        log_hdlr.PlgLogger.log(
            message="oAuth2 configuration converted as str to comply with QGIS "
            f"Authentication manager. Authentication configuration ID: {newAU.id} "
            f"({newAU.name})",
            log_level=3,
        )

        return newAU


class PlgOptionsManager:
    @staticmethod
    def get_plg_settings() -> PlgSettingsStructure:
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings
        :rtype: PlgSettingsStructure
        """
        # get dataclass fields definition
        settings_fields = fields(PlgSettingsStructure)

        # retrieve settings from QGIS/Qt
        settings = QgsSettings()
        settings.beginGroup(__title__)

        # map settings values to preferences object
        li_settings_values = []
        for i in settings_fields:
            try:
                value = settings.value(key=i.name, defaultValue=i.default, type=i.type)
                li_settings_values.append(value)
            except TypeError:
                li_settings_values.append(
                    settings.value(key=i.name, defaultValue=i.default)
                )

        # instanciate new settings object
        options = PlgSettingsStructure(*li_settings_values)

        settings.endGroup()

        return options

    @staticmethod
    def get_value_from_key(key: str, default=None, exp_type=None):
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings value matching key
        """
        if not hasattr(PlgSettingsStructure, key):
            log_hdlr.PlgLogger.log(
                message="Bad settings key. Must be one of: {}".format(
                    ",".join(PlgSettingsStructure._fields)
                ),
                log_level=1,
            )
            return None

        settings = QgsSettings()
        settings.beginGroup(__title__)

        try:
            out_value = settings.value(key=key, defaultValue=default, type=exp_type)
        except Exception as err:
            log_hdlr.PlgLogger.log(
                message="Error occurred trying to get settings: {}.Trace: {}".format(
                    key, err
                )
            )
            out_value = None

        settings.endGroup()

        return out_value

    @classmethod
    def set_value_from_key(cls, key: str, value) -> bool:
        """Set plugin QSettings value using the key.

        :param key: QSettings key
        :type key: str
        :param value: value to set
        :type value: depending on the settings
        :return: operation status
        :rtype: bool
        """
        if not hasattr(PlgSettingsStructure, key):
            log_hdlr.PlgLogger.log(
                message="Bad settings key. Must be one of: {}".format(
                    ",".join(PlgSettingsStructure._fields)
                ),
                log_level=2,
            )
            return False

        settings = QgsSettings()
        settings.beginGroup(__title__)

        try:
            settings.setValue(key, value)
            out_value = True
        except Exception as err:
            log_hdlr.PlgLogger.log(
                message="Error occurred trying to set settings: {}.Trace: {}".format(
                    key, err
                )
            )
            out_value = False

        settings.endGroup()

        return out_value

    @classmethod
    def save_from_object(cls, plugin_settings_obj: PlgSettingsStructure):
        """Load and return plugin settings as a dictionary. \
        Useful to get user preferences across plugin logic.

        :return: plugin settings value matching key
        """
        settings = QgsSettings()
        settings.beginGroup(__title__)

        for k, v in asdict(plugin_settings_obj).items():
            cls.set_value_from_key(k, v)
        settings.endGroup()
