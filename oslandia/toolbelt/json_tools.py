import json
from pathlib import Path


def json_to_dict(file_path: Path) -> dict:
    """
    Reads a JSON file and converts it to a Python dictionary.

    Args:
        file_path (str): The path to the JSON file.

    Returns:
        dict: A dictionary representing the data in the JSON file.
    """
    with file_path.open(encoding="utf-8") as json_file:
        data = json.load(json_file)
    return data


def get_key_by_name_in_dict(name: str, the_dict: dict, key: str) -> str:
    """This method searches for a plugin with the given name in the given dictionary
    and returns the value associated with the given key.

    :param name: The name of the plugin to search for.
    :type name: str
    :param the_dict: The dictionary containing the datas.
    :type the_dict: dict
    :param key: The key to search for in the plugin dictionary.
    :type key: str
    :return: The value associated with the given key in the plugin dictionary, or an empty string if the plugin is not found or the key is not present.
    :rtype: str
    """
    for dict_key in the_dict:
        if isinstance(the_dict[dict_key], list):
            for plugin in the_dict[dict_key]:
                if plugin["name"] == name:
                    return plugin.get(key, "")
    return ""
