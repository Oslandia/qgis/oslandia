from datetime import datetime
from functools import lru_cache
from typing import Union

from qgis.core import QgsSettings
from qgis.PyQt.QtCore import QDateTime, QLocale, Qt

# -- GLOBALS --
qlocale: QLocale = QLocale(QgsSettings().value("locale/userLocale", QLocale().name()))


@lru_cache()
def to_qdatetime(in_datetime: Union[datetime, str]) -> QDateTime:
    """Transform a python datetime to a QDateTime.

    :param in_datetime: _description_
    :type in_datetime: Union[datetime, str]

    :return: datetime in a Qt friendly format.
    :rtype: QDateTime
    """
    if isinstance(in_datetime, datetime):
        return QDateTime(
            in_datetime.year,
            in_datetime.month,
            in_datetime.day,
            in_datetime.hour,
            in_datetime.minute,
            in_datetime.second,
            in_datetime.microsecond // 1000,  # QDateTime uses milliseconds
        )
    elif isinstance(in_datetime, str):
        return QDateTime.fromString(in_datetime, Qt.ISODateWithMs)
    else:
        raise TypeError(f"Unsupported datetime type: {type(in_datetime)}")


@lru_cache()
def localize_qdatetime(in_qdatetime: QDateTime, date_format: str = "short") -> str:
    """Localize a QDateTime using the QGIS locale.

    :param in_qdatetime: QDateTime
    :type in_qdatetime: QDateTime
    :return: localized date
    :rtype: str
    """
    if date_format == "short":
        return qlocale.toString(in_qdatetime, QLocale.ShortFormat)
    else:
        return qlocale.toString(in_qdatetime, QLocale.LongFormat)
