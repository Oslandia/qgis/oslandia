from datetime import datetime, timedelta
from pathlib import Path
from typing import Union

from qgis.PyQt.QtGui import QIcon

from oslandia.datamodels.mdl_projects_browser import ProjectsObj
from oslandia.toolbelt import PlgLogger
from oslandia.toolbelt.cache_manager import CacheManager
from oslandia.toolbelt.network_manager import NetworkRequestsManager


class PluginManager:
    GITLAB_PLUGINS_REMOTE: str = (
        "https://oslandia.gitlab.io/qgis/oslandia/gitlab_plugins.json"
    )
    PRIVATE_PROJECTS_REMOTE: str = (
        "https://oslandia.gitlab.io/qgis/oslandia/private_plugins.json"
    )
    PUBLIC_PROJECTS_REMOTE: str = (
        "https://oslandia.gitlab.io/qgis/oslandia/public_plugins.json"
    )

    def __init__(self):
        """Initialization."""
        self.cache_manager = CacheManager(".oslandia", "cache")
        self.network_manager = NetworkRequestsManager()
        self.log = PlgLogger().log

        # cache path json
        self.PUBLIC_PROJECTS_LOCAL = Path(
            self.cache_manager.get_cache_path / "public_plugins.json"
        )
        self.PRIVATE_PROJECTS_LOCAL = Path(
            self.cache_manager.get_cache_path / "private_plugins.json"
        )
        self.GITLAB_PLUGINS_LOCAL = Path(
            self.cache_manager.get_cache_path / "gitlab_plugins.json"
        )

    def update_json_plugins(self):
        """Update the list of plugins in the cache manager if they are more than a week old."""
        if not self.cache_manager.ensure_cache_dir_exists():
            self.cache_manager.create_cache_dir()

        self.download_if_needed(self.PUBLIC_PROJECTS_LOCAL, self.PUBLIC_PROJECTS_REMOTE)
        self.download_if_needed(
            self.PRIVATE_PROJECTS_LOCAL, self.PRIVATE_PROJECTS_REMOTE
        )
        self.download_if_needed(self.GITLAB_PLUGINS_LOCAL, self.GITLAB_PLUGINS_REMOTE)

    @property
    def json_exists(self):
        """Check if json files exists."""

        if not Path(self.PUBLIC_PROJECTS_LOCAL).exists():
            return False

        if not Path(self.GITLAB_PLUGINS_REMOTE).exists():
            return False

        if not Path(self.PRIVATE_PROJECTS_REMOTE).exists():
            return False

        return True

    def force_update_json_plugins(self):
        """Update the list of plugins in the cache manager without regard to date."""
        if not self.cache_manager.ensure_cache_dir_exists():
            self.cache_manager.create_cache_dir()

        # Directly download without checking the file age
        self.network_manager.download_file_to(
            self.PUBLIC_PROJECTS_REMOTE, self.PUBLIC_PROJECTS_LOCAL
        )
        self.network_manager.download_file_to(
            self.PRIVATE_PROJECTS_REMOTE, self.PRIVATE_PROJECTS_LOCAL
        )
        self.network_manager.download_file_to(
            self.GITLAB_PLUGINS_REMOTE, self.GITLAB_PLUGINS_LOCAL
        )

    def download_if_needed(self, file_path: Path, url: str, days: int = 7) -> None:
        """Check if a download is required. A download is required if the file does not
        exist or is more than x days old (defined by days).

        :param file_path: Path file
        :type file_path: Path
        :param url: URL to download
        :type url: str
        :param days: File expiration time, defaults to 7
        :type days: int, optional
        """
        if not file_path.exists() or datetime.now() - datetime.fromtimestamp(
            file_path.stat().st_mtime
        ) >= timedelta(days=days):
            self.network_manager.download_file_to(url, file_path)
        else:
            self.log(message=f"File {file_path} is up to date", log_level=0)

    def get_last_modification_date(self, file_path: Path) -> datetime:
        """Return the last modification date of a file.

        :param file_path: Path to the file
        :type file_path: Path
        :return: The datetime of the last modification
        :rtype: datetime
        :raises FileNotFoundError: If the file does not exist
        """
        if not file_path.exists():
            raise FileNotFoundError("The specified file does not exist.")
        return datetime.fromtimestamp(file_path.stat().st_mtime)

    @property
    def get_date_json_plugins_update(self) -> str:
        """Check the last modification date of a specific file.

        :param file_name: Name of the file to check
        :type file_name: str
        :return: Last modification date and time as a formatted string
        :rtype: str
        :raises FileNotFoundError: If the file does not exist
        """
        file_path = Path(self.PUBLIC_PROJECTS_LOCAL)
        last_modification_date = self.get_last_modification_date(file_path)
        return last_modification_date.strftime("%Y-%m-%d %H:%M:%S")

    def download_plugins_icons_to_cache(self) -> None:
        """Download plugins icons to cache."""
        projects_obj = ProjectsObj(self.cache_manager.get_cache_path)
        valid_extensions = [".png", ".svg", ".jpeg", ".jpg"]

        for project in projects_obj.get_projects():
            if project.logo:
                extension = next(
                    (ext for ext in valid_extensions if ext in project.logo), None
                )
                if extension:
                    icon_file = project.name.lower().replace(" ", "_") + extension
                    icon_path = Path(
                        self.cache_manager.get_cache_path / "icons" / icon_file
                    )
                    if not icon_path.exists():
                        self.network_manager.download_file_to(project.logo, icon_path)

    def get_project_icon(self, project_name: str, logo: str) -> Union[QIcon, None]:
        """Generate an icon for the project based on its logo path.

        :param project_name: Name of the project
        :type project_name: str
        :param logo: Path or name of the logo file
        :type logo: str
        :return: QIcon if logo extension is valid, otherwise None
        :rtype: Optional[QIcon]
        """
        valid_extensions = (".png", ".svg", ".jpg", ".jpeg")
        extension = next((ext for ext in valid_extensions if ext in logo), None)

        if extension is None:
            return None

        icon_file = project_name.lower().replace(" ", "_") + extension
        icon_path = Path(self.cache_manager.get_cache_path / "icons" / icon_file)

        return QIcon(str(icon_path))
