from typing import Optional

from qgis.PyQt.QtCore import QObject, Qt
from qgis.PyQt.QtGui import QIcon, QStandardItemModel

from oslandia.gitlab_api.project import Project, ProjectRequestManager
from oslandia.toolbelt import PlgLogger
from oslandia.toolbelt.qt_utils import to_qdatetime


class ProjectListModel(QStandardItemModel):
    ID = 0
    DATE_COL = 1
    NAME_COL = 2
    ARCHIVED_COL = 3

    def __init__(self, parent: Optional[QObject] = None):
        """
        QStandardItemModel for datastore list display

        Args:
            parent: QObject parent
        """
        super().__init__(parent)
        self.log = PlgLogger().log
        self.setHorizontalHeaderLabels(
            [
                self.tr("Id"),
                self.tr("Date"),
                self.tr("Name"),
                self.tr("Archived"),
            ]
        )

    def flags(self, index):
        default_flags = super().flags(index)
        return default_flags & ~Qt.ItemIsEditable  # Disable editing

    def set_group(self, group: str) -> None:
        """
        Refresh QStandardItemModel data with current group project

        """
        self.removeRows(0, self.rowCount())

        manager = ProjectRequestManager()
        self.log(f"{group}", log_level=2)
        projects = manager.get_project_list(group)
        for project in projects:
            manager.download_avatar(project)
            self.insert_project(project)

    def insert_project(self, project: Project) -> None:
        """Insert project into model

        :param project: project to insert
        :type project: Project
        """
        row = self.rowCount()
        self.insertRow(row)
        self.setData(self.index(row, self.ID), project.id)

        self.setData(
            self.index(row, self.DATE_COL),
            to_qdatetime(project.created_at),
        )
        self.setData(self.index(row, self.NAME_COL), project.name)
        self.setData(self.index(row, self.NAME_COL), project, Qt.UserRole)
        self.setData(self.index(row, self.ARCHIVED_COL), project.archived)
        self.setData(
            self.index(row, self.NAME_COL),
            QIcon(f"{project.avatar_local_filepath.resolve()}"),
            Qt.DecorationRole,
        )

    def get_project_row(self, project_id: str) -> int:
        """Return model row for a project id. -1 if project is not available

        :param project_id: project unique identifier
        :type project_id: str

        :return: row model matching project ID
        :rtype: int
        """
        for row in range(self.rowCount()):
            if self.data(self.index(row, self.ID)) == project_id:
                return row
        return -1
