from qgis.PyQt.QtCore import QObject, Qt
from qgis.PyQt.QtGui import QStandardItemModel

from oslandia.gitlab_api.labels import Labels, LabelsRequestManager
from oslandia.toolbelt import PlgLogger


class LabelsListModel(QStandardItemModel):
    NAME_COL = 0
    DESCRIPTION_COL = 1

    def __init__(self, parent: QObject = None):
        """
        QStandardItemModel for datastore list display

        Args:
            parent: QObject parent
        """
        super().__init__(parent)
        self.log = PlgLogger().log
        self.setHorizontalHeaderLabels([self.tr("Name"), self.tr("Description")])

    def flags(self, index):
        default_flags = super().flags(index)
        return default_flags & ~Qt.ItemIsEditable  # Disable editing

    def set_project(self, project_id: str) -> None:
        """
        Refresh QStandardItemModel data with current project_id labels

        """
        self.removeRows(0, self.rowCount())
        manager = LabelsRequestManager()
        labels = manager.get_labels_list(project_id)
        for label in labels:
            self.insert_labels(label)

    def insert_labels(self, labels: Labels) -> None:
        """Insert labels into model

        :param labels: labels to insert
        :type labels: labels
        """
        row = self.rowCount()
        self.insertRow(row)
        self.setData(self.index(row, self.NAME_COL), labels.name)
        self.setData(self.index(row, self.NAME_COL), labels, Qt.UserRole)
        self.setData(self.index(row, self.DESCRIPTION_COL), labels.description)
