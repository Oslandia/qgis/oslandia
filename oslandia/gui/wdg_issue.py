"""Widget for issue display
"""

from datetime import datetime

# standard
from enum import Enum
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtCore import QSortFilterProxyModel, Qt
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.discussions import Discussion, DiscussionsRequestManager
from oslandia.gitlab_api.issue import Issues, IssuesRequestManager
from oslandia.gitlab_api.labels import LabelsRequestManager
from oslandia.gui.mdl_members import MembersListModel
from oslandia.gui.wdg_discussion import DiscussionWidget


class IssueWidget(QWidget):
    """QWidget to display issue

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    class DisplayMode(Enum):
        READ = 1
        UPDATE = 2
        CREATE = 3

    # Signal to indicate changes in issue displayed
    issueChanged = QtCore.pyqtSignal()

    # Signal to indicate that the issue was updated
    issueUpdated = QtCore.pyqtSignal(Issues)

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_issue.ui"
        uic.loadUi(ui_path, self)

        self.issue = None

        self.mdl_members = MembersListModel(self)
        self.mdl_members.sort(MembersListModel.NAME_COL)

        self.proxy_mdl_members = QSortFilterProxyModel(self)
        self.proxy_mdl_members.setSourceModel(self.mdl_members)
        self.proxy_mdl_members.setSortCaseSensitivity(Qt.CaseInsensitive)
        self.proxy_mdl_members.sort(
            MembersListModel.NAME_COL, Qt.SortOrder.AscendingOrder
        )
        self.cbx_assignee.setModel(self.proxy_mdl_members)

        # QgsCheckableComboBox doesn't support model use

        self.lne_title.textChanged.connect(self.issueChanged.emit)
        self.txe_description.textChanged.connect(self.issueChanged.emit)
        self.cbx_labels.currentIndexChanged.connect(self.issueChanged.emit)
        self.cbx_assignee.currentIndexChanged.connect(self.issueChanged.emit)

        self.txe_description.setPlaceholderText(self.tr("Description *"))

        self.wdg_note_creation.discussionAdded.connect(self._add_discussion)

        self.set_display_mode(IssueWidget.DisplayMode.READ)

        self.btn_edit.clicked.connect(self._update_mode)
        self.btn_cancel.clicked.connect(self._cancel_update)
        self.btn_save.clicked.connect(self._update_issue)

        self.btn_edit.setIcon(QgsApplication.getThemeIcon("mActionToggleEditing.svg"))
        self.btn_cancel.setIcon(
            QgsApplication.getThemeIcon("mActionCancelAllEdits.svg")
        )
        self.btn_save.setIcon(QgsApplication.getThemeIcon("mActionSaveAllEdits.svg"))

    def _update_mode(self) -> None:
        """Switch to UPDATE mode"""
        self.set_display_mode(IssueWidget.DisplayMode.UPDATE)

    def _cancel_update(self) -> None:
        """Cancel UPDATE mode and restore displayed issue"""
        if self.issue:
            self.set_issue(self.issue)
        self.set_display_mode(IssueWidget.DisplayMode.READ)

    def _update_issue(self) -> None:
        """Update issue with current displayed values"""
        issue = self.get_issue()
        issue_manager = IssuesRequestManager()
        issue_manager.update_issue(issue)
        self.issue = issue
        self.set_display_mode(IssueWidget.DisplayMode.READ)
        self.issueUpdated.emit(issue)

    def is_issue_changed(self) -> bool:
        """Check if displayed issue is changed from input issue

        :return: True if issue was changed, False otherwise
        :rtype: bool
        """
        if self.issue:
            return self.issue != self.get_issue()
        return False

    def get_display_mode(self) -> DisplayMode:
        """Return current display mode

        :return: current display mode
        :rtype: DisplayMode
        """
        return self.display_mode

    def set_display_mode(self, mode: DisplayMode) -> None:
        """Set display mode:
            IssueWidget.DisplayMode.READ : all GUI are read only
            IssueWidget.DisplayMode.UPDATE : user can edit GUI
            IssueWidget.DisplayMode.CREATE : user can edit GUI but comment are not displayed

        :param mode: display mode
        :type mode: DisplayMode
        """
        self.display_mode = mode
        # Read only attribute
        read_only = mode == IssueWidget.DisplayMode.READ
        self.lne_title.setReadOnly(read_only)
        self.txe_description.setReadOnly(read_only)
        self.cbx_labels.setAttribute(Qt.WA_TransparentForMouseEvents, read_only)
        self.cbx_labels.setFocusPolicy(Qt.NoFocus if read_only else Qt.StrongFocus)
        self.cbx_assignee.setAttribute(Qt.WA_TransparentForMouseEvents, read_only)
        self.cbx_assignee.setFocusPolicy(Qt.NoFocus if read_only else Qt.StrongFocus)

        # Don't display comment in CREATE mode
        self.grp_comment.setVisible(mode != IssueWidget.DisplayMode.CREATE)

        self.btn_edit.setVisible(mode == IssueWidget.DisplayMode.READ)
        self.btn_save.setVisible(mode == IssueWidget.DisplayMode.UPDATE)
        self.btn_cancel.setVisible(mode == IssueWidget.DisplayMode.UPDATE)

    def set_current_project_id(self, project_id: str) -> None:
        """Define current project to define labels and members available

        :param project_id: _description_
        :type project_id: str
        """
        label_manager = LabelsRequestManager()
        labels = label_manager.get_labels_list(project_id=project_id)
        self.cbx_labels.clear()
        for label in labels:
            self.cbx_labels.addItemWithCheckState(label.name, Qt.CheckState.Unchecked)

        self.mdl_members.set_project(project_id)
        self.proxy_mdl_members.sort(
            MembersListModel.NAME_COL, Qt.SortOrder.AscendingOrder
        )
        self.txe_description.set_project_id(project_id)

    @staticmethod
    def clear_layout(layout):
        """Clear a layout from all added widget

        :param layout: layout to clear
        :type layout: any Qt type layouy
        """
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    layout.removeItem(item)

    def set_issue(self, issue: Issues) -> None:
        """Set issue displayed

        :param issue: issue
        :type issue: Issues
        """
        self.issue = issue
        self.lne_title.setText(issue.name)
        self.txe_description.setText(issue.description)
        self.txe_description.set_project_id(issue.project_id)

        self.cbx_labels.deselectAllOptions()
        self.cbx_labels.setCheckedItems(issue.labels)

        for row in range(self.mdl_members.rowCount()):
            index = self.proxy_mdl_members.index(row, self.mdl_members.NAME_COL)
            member = self.proxy_mdl_members.data(
                index,
                Qt.UserRole,
            )
            if issue.assignee_id:
                if member and member.id == issue.assignee_id:
                    self.cbx_assignee.setCurrentIndex(row)
                    break
            elif member is None:
                self.cbx_assignee.setCurrentIndex(row)
                break

        # A discussions as note only if not in CREATE mode
        if self.display_mode != IssueWidget.DisplayMode.CREATE:
            self._refresh_notes()

        self.wdg_note_creation.set_issue(issue)

    def _refresh_notes(self) -> None:
        """Refresh notes for current displayed issue"""
        self.clear_layout(self.lyt_notes)
        if self.issue:
            discussion_manager = DiscussionsRequestManager()
            discussions = discussion_manager.get_discussions_list(
                self.issue.project_id, self.issue.iid
            )
            for discussion in discussions:
                self._add_discussion(discussion)

    def _add_discussion(self, discussion: Discussion) -> None:
        # For now only display discussion that are not system notes
        not_system_note = [note for note in discussion.notes if not note.system]
        if len(not_system_note) != 0:
            discussion_widget = DiscussionWidget(self)
            discussion_widget.set_discussion(discussion)
            self.lyt_notes.addWidget(discussion_widget)

    def _selected_assignee_id(self) -> Optional[int]:
        """Get selected assignee id

        :return: selected assignee id
        :rtype: Optional[int]
        """
        assignee_current_index = self.cbx_assignee.currentIndex()
        if assignee_current_index != -1:
            member = self.mdl_members.data(
                self.proxy_mdl_members.mapToSource(
                    self.proxy_mdl_members.index(
                        assignee_current_index, MembersListModel.NAME_COL
                    )
                ),
                Qt.UserRole,
            )
            if member:
                return member.id
        return None

    def get_issue(self) -> Issues:
        """Get issue displayed

        :return: issue
        :rtype: Issues
        """
        return Issues(
            id=self.issue.id if self.issue else "",
            project_id=self.issue.project_id if self.issue else "",
            name=self.lne_title.text(),
            description=self.txe_description.toPlainText(),
            state=self.issue.state if self.issue else "",
            created_date=self.issue.created_date if self.issue else datetime.now(),
            assignee_id=self._selected_assignee_id(),
            labels=self.cbx_labels.checkedItems(),
            web_url=self.issue.web_url if self.issue else "",
            iid=self.issue.iid if self.issue else "",
        )
