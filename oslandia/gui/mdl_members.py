from qgis.PyQt.QtCore import QObject, Qt
from qgis.PyQt.QtGui import QStandardItemModel

from oslandia.gitlab_api.members import Members, MembersRequestManager
from oslandia.toolbelt import PlgLogger
from oslandia.toolbelt.preferences import PlgOptionsManager


class MembersListModel(QStandardItemModel):
    NAME_COL = 0
    # AVATAR_IMAGE = 1

    def __init__(self, parent: QObject = None, allow_null_value: bool = True):
        """
        QStandardItemModel for datastore list display

        Args:
            parent: QObject parent
            allow_null_value: bool : allow null value in model
        """
        super().__init__(parent)
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager.get_plg_settings()

        self.setHorizontalHeaderLabels([self.tr("Name")])

        self.allow_null_value = allow_null_value

    def flags(self, index):
        default_flags = super().flags(index)
        return default_flags & ~Qt.ItemIsEditable  # Disable editing

    def set_project(self, project_id: str) -> None:
        """
        Refresh QStandardItemModel data with current project_id members

        """
        self.removeRows(0, self.rowCount())
        if self.allow_null_value:
            row = self.rowCount()
            self.insertRow(row)

        manager = MembersRequestManager()
        members = manager.get_members_list(project_id)
        for member in members:
            self.insert_members(member)

    """
    For now seems to be impossible to download avatar image
    --> https://gitlab.com/gitlab-org/gitlab/-/issues/441353
    """
    # def download_image(self, url, name, size=(30, 30)):

    # response = requests.get(url)
    # if response.status_code == 200:
    #     image_data = BytesIO(response.content)
    #     pixmap = QPixmap()
    #     pixmap.loadFromData(image_data.read())
    #     pixmap = pixmap.scaled(
    #         size[0], size[1], Qt.KeepAspectRatio, Qt.SmoothTransformation
    #     )
    #     return pixmap
    # else:
    #     return QPixmap()

    def insert_members(self, members: Members) -> None:
        """Insert members into model

        :param members: members to insert
        :type members: members

        """
        row = self.rowCount()
        self.insertRow(row)
        self.setData(self.index(row, self.NAME_COL), members.name)
        self.setData(self.index(row, self.NAME_COL), members, Qt.UserRole)

        # pixmap = self.download_image(members.avatar_url, members.name)
        # self.setData(self.index(row, self.AVATAR_IMAGE), pixmap, Qt.DecorationRole)
