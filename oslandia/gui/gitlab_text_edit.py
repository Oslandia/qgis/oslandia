""" A text editor that automatically upload file to gitlab at drop """

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication
from qgis.PyQt.QtGui import QDragEnterEvent, QDropEvent, QIcon
from qgis.PyQt.QtWidgets import QAction, QFileDialog, QWidget

# project
from oslandia.gitlab_api.upload import UploadRequestManager
from oslandia.gui.auto_resizing_text_edit import AutoResizingTextEdit


class GitLabTextEdit(AutoResizingTextEdit):
    def __init__(self, parent: Optional[QWidget] = None) -> None:
        super().__init__(parent)
        self._project_id = None
        self.setAcceptDrops(True)

        self._insert_separator_in_edit_action()
        attach_file_action = QAction(
            QIcon(QgsApplication.iconPath("mActionEditInsertLink.svg")),
            self.tr("Attach file"),
        )
        attach_file_action.setToolTip(self.tr("Attach file or image."))
        attach_file_action.triggered.connect(self._insert_file)
        self._edit_actions.append(attach_file_action)

    def _insert_file(self) -> None:
        """Select file and insert to edit widget"""
        file_path, _ = QFileDialog.getOpenFileName(
            self, self.tr("Select file to upload")
        )
        if file_path:
            self.insert_file(file_path)

    def set_project_id(self, _project_id: str) -> None:
        """Define project_id needed for image displau

        :param _project_id: project id
        :type _project_id: str
        """
        self._project_id = _project_id

    def dragEnterEvent(self, event: QDragEnterEvent) -> None:
        """Check if we accept drag event, only urls are accepted

        :param event: drag enter event
        :type event: QDragEnterEvent
        """
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            event.ignore()

    def dropEvent(self, event: QDropEvent) -> None:
        """Accept drop event for url and create upload in GitLab for current project

        :param event: drop event
        :type event: QDropEvent
        """
        if not self._project_id:
            event.ignore()
            return

        if event.mimeData().hasUrls():
            urls = event.mimeData().urls()
            file_paths = [url.toLocalFile() for url in urls]

            for path in file_paths:
                self.insert_file(path)
            event.acceptProposedAction()
        else:
            event.ignore()

    def insert_file(self, file_path: Path) -> None:
        """Insert file in document.
        Upload file to project and insert markdown string

        :param file_path: file path
        :type file_path: Path
        """
        if not self._project_id:
            return
        manager = UploadRequestManager()
        md_str = manager.create_upload(self._project_id, file_path)
        if md_str:
            self.textCursor().insertText(md_str)
            self.textCursor().insertText("\n")
