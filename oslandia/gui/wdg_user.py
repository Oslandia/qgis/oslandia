"""Widget for user display
"""

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QPixmap
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.user import User


class UserWidget(QWidget):
    """QWidget to display user

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_user.ui"
        uic.loadUi(ui_path, self)

    def set_user(self, user: User) -> None:
        """Set user displayed

        :param user: user
        :type user: User
        """
        self.lbl_user_name_value.setText(user.username)
        self.lbl_name_value.setText(user.name)
        self.lbl_email_value.setText(user.email)

        if user.avatar_local_filepath.exists():
            user_avatar_pixmap = QPixmap(str(user.avatar_local_filepath.resolve()))
            self.lbl_avatar.setPixmap(user_avatar_pixmap)
