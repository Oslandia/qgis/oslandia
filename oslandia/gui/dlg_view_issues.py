"""Dialog for issue view
"""

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QItemSelection, QItemSelectionModel, Qt
from qgis.PyQt.QtWidgets import QAbstractItemView, QDialog, QMessageBox, QWidget
from qgis.utils import OverrideCursor

# project
from oslandia.gitlab_api.group import GroupRequestManager
from oslandia.gitlab_api.issue import Issues
from oslandia.gitlab_api.project import ProjectRequestManager
from oslandia.gitlab_api.user import UserRequestManager
from oslandia.gui.delegate_issue_table_view import IssueTableViewDelegate
from oslandia.gui.dlg_create_issue import CreateIssueDialog
from oslandia.gui.mdl_issue import IssuesListModel
from oslandia.gui.mdl_project import ProjectListModel
from oslandia.gui.proxy_model_issue import IssueProxyModel
from oslandia.gui.proxy_model_project import ProjectProxyModel
from oslandia.gui.wdg_issue import IssueWidget
from oslandia.toolbelt.network_manager import NetworkRequestsManager
from oslandia.toolbelt.preferences import PlgOptionsManager


class ViewIssueDialog(QDialog):
    """QDialog to display issue from a selected project

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        with OverrideCursor(Qt.WaitCursor):
            ui_path = Path(__file__).resolve(True).parent / "dlg_view_issues.ui"
            uic.loadUi(ui_path, self)
            self.setWindowTitle(self.tr("Project issues"))
            self.plg_settings = PlgOptionsManager.get_plg_settings()
            self.ntwk_requester_blk = NetworkRequestsManager()
            self.project_request = ProjectRequestManager()
            self.mdl_project = ProjectListModel(self)
            self.user_request_manager = UserRequestManager()

            assistance_group = GroupRequestManager().get_group_from_id(
                self.plg_settings.gitlab_group_id
            )
            if assistance_group:
                self.mdl_project.set_group(assistance_group.id)

            self.proxy_mdl = ProjectProxyModel(self)
            self.proxy_mdl.setSourceModel(self.mdl_project)
            self.proxy_mdl.setSortCaseSensitivity(Qt.CaseInsensitive)
            self.proxy_mdl.sort(ProjectListModel.NAME_COL, Qt.SortOrder.AscendingOrder)
            self.cbx_projects.setModel(self.proxy_mdl)
            self.cbx_projects.setModelColumn(ProjectListModel.NAME_COL)
            self.proxy_mdl.show_archived(self.ckb_display_archived.isChecked())

            self.mdl_issue = IssuesListModel(self)
            self.proxy_mdl_issue = IssueProxyModel(self)
            self.proxy_mdl_issue.setSourceModel(self.mdl_issue)
            self.proxy_mdl_issue.show_closed(self.ckb_display_closed.isChecked())

            self.tbv_issues.setModel(self.proxy_mdl_issue)
            self.tbv_issues.setSelectionMode(
                QAbstractItemView.SelectionMode.SingleSelection
            )
            self.tbv_issues.setSelectionBehavior(
                QAbstractItemView.SelectionBehavior.SelectRows
            )

            self.ckb_display_archived.clicked.connect(self._project_filter_changed)
            self.ckb_display_closed.clicked.connect(self._issue_filter_changed)
            self.ckb_assign_me.clicked.connect(self._issue_filter_changed)
            self.lne_search.textChanged.connect(self._issue_filter_changed)
            self.cbx_projects.currentIndexChanged.connect(self._project_changed)

            self.tbv_issues.selectionModel().selectionChanged.connect(
                self._selected_issue_changed
            )

            self.tbv_issues.verticalHeader().setVisible(False)
            self.tbv_issues.horizontalHeader().setStretchLastSection(True)

            delegate = IssueTableViewDelegate(self.tbv_issues)
            self.tbv_issues.setItemDelegate(delegate)

            self.btn_refresh.setIcon(QgsApplication.getThemeIcon("mActionRefresh.svg"))
            self.btn_refresh.clicked.connect(self._refresh_issues)

            self.btn_issue_add.setIcon(QgsApplication.getThemeIcon("mActionAdd.svg"))
            self.btn_issue_add.clicked.connect(self._add_issue)

            self.wdg_issue.set_display_mode(IssueWidget.DisplayMode.READ)
            self.wdg_issue.issueUpdated.connect(self._issue_updated)

            self.lbl_search.setToolTip(
                self.tr("Filter on issue title and description.")
            )
            # Simulate project changes for project_id definition in widgets
            self._project_changed()
            # make the dialog a window to allow resizing
            self.setWindowFlags(Qt.Window)

    def _project_filter_changed(self) -> None:
        """Update project filter model"""
        with OverrideCursor(Qt.WaitCursor):
            self.proxy_mdl.show_archived(self.ckb_display_archived.isChecked())

    def _issue_filter_changed(self) -> None:
        """Update project filter model"""
        with OverrideCursor(Qt.WaitCursor):
            self.proxy_mdl_issue.show_closed(self.ckb_display_closed.isChecked())
            user_id = (
                [self.user_request_manager.get_current_user().id]
                if self.ckb_assign_me.isChecked()
                else []
            )
            self.proxy_mdl_issue.set_visible_user(user_id)
            search_text = None
            if self.lne_search.text():
                search_text = self.lne_search.text()
            self.proxy_mdl_issue.set_search_txt(search_text)

    def _project_changed(self) -> None:
        """Update issue widget for current project"""
        with OverrideCursor(Qt.WaitCursor):
            project_id = self._selected_project_id()
            self.mdl_issue.set_project(project_id)
            self.wdg_issue.set_current_project_id(project_id)

    def _issue_updated(self, issue: Issues) -> None:
        """Update current selected row issue

        :param issue: updated issue
        :type issue: Issues
        """
        selected_index = self.tbv_issues.selectionModel().selectedRows()
        if selected_index:
            index = self.proxy_mdl_issue.mapToSource(selected_index[0])
            self.mdl_issue.set_row_issue(index.row(), issue)

    def _refresh_issues(self) -> None:
        """Refresh visible issues"""
        # Simulate a project changes
        self._project_changed()

    def _add_issue(self) -> None:
        """Add issue for selected project"""
        project_id = self._selected_project_id()
        dlg_create_issue = CreateIssueDialog(self)
        dlg_create_issue.set_current_project_id(project_id)
        dlg_create_issue.setModal(False)
        if dlg_create_issue.exec():
            self._refresh_issues()

    def _selected_project_id(self) -> str:
        """Get selected project id

        :return: selected project id
        :rtype: str
        """
        project_index = self.cbx_projects.currentIndex()
        return self.mdl_project.data(
            self.proxy_mdl.mapToSource(
                self.proxy_mdl.index(project_index, ProjectListModel.ID)
            )
        )

    def _selected_issue_changed(
        self, selected: QItemSelection, deselected: QItemSelection
    ) -> None:
        """Update visible issue"""

        # Check if current issue is updated
        if (
            self.wdg_issue.get_display_mode() == IssueWidget.DisplayMode.UPDATE
            and self.wdg_issue.is_issue_changed()
        ):
            msg_box = QMessageBox(self)
            msg_box.setIcon(QMessageBox.Icon.Information)
            msg_box.setText(
                self.tr(
                    "Updated issue won't be saved. Do you want to change displayed issue ?"
                )
            )
            msg_box.setWindowTitle(self.tr("Changes unsaved"))
            msg_box.setStandardButtons(
                QMessageBox.StandardButton.Ok | QMessageBox.StandardButton.Cancel
            )

            # Restore previous selection
            if msg_box.exec() != QMessageBox.StandardButton.Ok:
                self.tbv_issues.selectionModel().selectionChanged.disconnect(
                    self._selected_issue_changed
                )
                self.tbv_issues.selectionModel().select(
                    deselected, QItemSelectionModel.ClearAndSelect
                )
                self.tbv_issues.selectionModel().selectionChanged.connect(
                    self._selected_issue_changed
                )
                return

        with OverrideCursor(Qt.WaitCursor):
            selected_index = self.tbv_issues.selectionModel().selectedRows()
            if selected_index:
                issue = self.mdl_issue.data(
                    self.proxy_mdl_issue.mapToSource(
                        self.proxy_mdl_issue.index(
                            selected_index[0].row(), IssuesListModel.NAME_COL
                        )
                    ),
                    Qt.UserRole,
                )
                self.wdg_issue.set_issue(issue)
                self.wdg_issue.set_display_mode(IssueWidget.DisplayMode.READ)
