# standard
from pathlib import Path

# PyQGIS
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget

# plugin
from oslandia.toolbelt import PlgLogger, PlgOptionsManager


class IssueContextAuthorisationWidget(QWidget):
    def __init__(self, parent: QWidget = None):
        """QWidget to set issue context authorisation.

        :param parent: parent widget or application
        :type parent: QWidget
        """
        super().__init__(parent)
        self.log = PlgLogger().log
        uic.loadUi(Path(__file__).parent / f"{Path(__file__).stem}.ui", self)

        # fill fields from saved settings
        self.load_settings()

    def load_settings(self) -> None:
        """Load options from QgsSettings into UI form."""
        plg_settings = PlgOptionsManager()
        settings = plg_settings.get_plg_settings()

        self.ckb_send_qgis_info.setChecked(settings.allow_qgis_info_add_in_new_issue)
        self.ckb_send_plugin_info.setChecked(
            settings.allow_plugin_info_add_in_new_issue
        )

    def save_settings(self) -> None:
        """Save form text into QgsSettings."""
        # get settings
        plg_settings = PlgOptionsManager()
        settings = plg_settings.get_plg_settings()

        # store user inputs
        settings.allow_qgis_info_add_in_new_issue = self.ckb_send_qgis_info.isChecked()
        settings.allow_plugin_info_add_in_new_issue = (
            self.ckb_send_plugin_info.isChecked()
        )

        # save it
        plg_settings.save_from_object(settings)

    def is_send_qgis_info_enabled(self) -> bool:
        """Check if QGIS info send is enabled

        :return: True if QGIS info send is enabled, False otherwise
        :rtype: bool
        """
        return self.ckb_send_qgis_info.isChecked()

    def is_send_plugin_info_enabled(self) -> bool:
        """Check if plugin info send is enabled

        :return: True if plugin info send is enabled, False otherwise
        :rtype: bool
        """
        return self.ckb_send_plugin_info.isChecked()
