"""Widget for discussion display
"""

# standard
from enum import Enum
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.discussions import Discussion, Note
from oslandia.gui.wdg_note import NoteWidget
from oslandia.toolbelt.qt_utils import localize_qdatetime, to_qdatetime


class DiscussionWidget(QWidget):
    """QWidget to display discussion

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    # Signal to indicate changes in discussion displayed
    discussionChanged = QtCore.pyqtSignal()

    # Signal to indicate that the discussion was updated
    discussionUpdated = QtCore.pyqtSignal()

    class DisplayMode(Enum):
        READ = 1
        UPDATE = 2

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_discussion.ui"
        uic.loadUi(ui_path, self)
        self.discussion = None

        self.note_widget.set_display_mode(NoteWidget.DisplayMode.READ)
        self.note_widget.noteChanged.connect(self.discussionChanged)
        self.note_widget.noteUpdated.connect(self.discussionUpdated)
        self.reply_widget.noteReplyAdded.connect(self._add_reply)

    def set_discussion(self, discussion: Discussion) -> None:
        """Set note displayed

        :param note: note
        :type note: Note
        """
        self.discussion = discussion
        self.reply_widget.set_discussion(self.discussion)
        first_note = True
        for note in discussion.notes:
            if not note.system:
                # First note is a comment
                if first_note:
                    self.note_widget.set_note(note)
                    first_note = False
                # Other notes are reply
                else:
                    self._add_reply(note)

    def _add_reply(self, reply: Note) -> None:
        """Add a reply in discussion

        :param reply: reply
        :type reply: Note
        """
        reply_widget = NoteWidget(self)
        reply_widget.set_note(reply)
        reply_widget.set_display_mode(NoteWidget.DisplayMode.READ)
        reply_widget.noteChanged.connect(self.discussionChanged)
        reply_widget.noteUpdated.connect(self.discussionUpdated)
        # Always insert before NoteReplyCreationWidget
        self.lyt_replies.insertWidget(self.lyt_replies.count() - 1, reply_widget)
        nb_replies = self.lyt_replies.count() - 1
        grp_text = self.tr(
            "{} reply Last reply by {} - {}".format(
                nb_replies,
                reply.author.name,
                localize_qdatetime(to_qdatetime(reply.created_at)),
            )
        )

        self.grp_replies.setTitle(grp_text)
        self.grp_replies.setToolTip(
            localize_qdatetime(to_qdatetime(reply.created_at), date_format="long")
        )
