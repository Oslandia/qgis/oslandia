"""Widget for note reply creation
"""

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.discussions import Discussion, DiscussionsRequestManager, Note


class NoteReplyCreationWidget(QWidget):
    """QWidget to create note reply

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    # Signal to indicate that a note reply was added
    noteReplyAdded = QtCore.pyqtSignal(Note)

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_note_reply_creation.ui"
        uic.loadUi(ui_path, self)
        self.discussion = None
        self.btn_reply.setEnabled(False)
        self.btn_reply.clicked.connect(self._reply)

        self.txt_body.setStyleSheet(
            """
            QTextEdit {
                border-radius: 15px;
                border: 2px solid lightblue;
                padding: 10px;
                background-color: white;
            }
        """
        )

    def set_discussion(self, discussion: Discussion) -> None:
        """Define discussion used to add note

        :param discussion: discussion
        :type discussion: Discussion
        """
        self.discussion = discussion
        self.btn_reply.setEnabled(True)
        self.txt_body.set_project_id(discussion.project_id)

    def _reply(self) -> None:
        """Add comment to current discussion from displayed text"""
        if self.discussion:
            manager = DiscussionsRequestManager()
            note = manager.add_note_reply(
                project_id=self.discussion.project_id,
                issue_iid=self.discussion.issue_iid,
                discussion_id=self.discussion.id,
                body=self.txt_body.toPlainText(),
            )
            self.txt_body.clear()
            self.noteReplyAdded.emit(note)
