"""Widget for note creation
"""

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.discussions import Discussion
from oslandia.gitlab_api.issue import Issues, IssuesRequestManager


class NoteCreationWidget(QWidget):
    """QWidget to create note

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    # Signal to indicate that a note was added
    discussionAdded = QtCore.pyqtSignal(Discussion)

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_note_creation.ui"
        uic.loadUi(ui_path, self)
        self.issue = None
        self.btn_comment.setEnabled(False)
        self.btn_comment.clicked.connect(self._comment)

        self.txt_body.setStyleSheet(
            """
            QTextEdit {
                border-radius: 15px;
                border: 2px solid lightblue;
                padding: 10px;
                background-color: white;
            }
        """
        )

    def set_issue(self, issue: Issues) -> None:
        """Define issue used to add note

        :param issue: issue
        :type issue: Issues
        """
        self.issue = issue
        self.btn_comment.setEnabled(True)
        self.txt_body.set_project_id(issue.project_id)

    def _comment(self) -> None:
        """Add comment to current issue from displayed text"""
        if self.issue:
            manager = IssuesRequestManager()
            discussion = manager.create_issue_note(
                self.issue.project_id, self.issue.iid, self.txt_body.toPlainText()
            )
            self.txt_body.clear()
            self.discussionAdded.emit(discussion)
