"""Widget for note display
"""

# standard
from enum import Enum
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication
from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtWidgets import QWidget

# project
from oslandia.gitlab_api.discussions import Note
from oslandia.gitlab_api.issue import IssuesRequestManager
from oslandia.toolbelt.qt_utils import localize_qdatetime, to_qdatetime


class NoteWidget(QWidget):
    """QWidget to display note

    :param parent: dialog parent, defaults to None
    :type parent: Optional[QWidget], optional
    """

    # Signal to indicate changes in note displayed
    noteChanged = QtCore.pyqtSignal()

    # Signal to indicate that the note was updated
    noteUpdated = QtCore.pyqtSignal()

    class DisplayMode(Enum):
        READ = 1
        UPDATE = 2

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        ui_path = Path(__file__).resolve(True).parent / "wdg_note.ui"
        uic.loadUi(ui_path, self)

        self.note = None

        self.txt_body.setStyleSheet(
            """
            QTextEdit {
                border-radius: 15px;
                border: 2px solid lightblue;
                padding: 10px;
                background-color: white;
            }
        """
        )

        self.txt_body.textChanged.connect(self.noteChanged.emit)
        self.set_display_mode(NoteWidget.DisplayMode.READ)

        self.btn_edit.clicked.connect(self._update_mode)
        self.btn_cancel.clicked.connect(self._cancel_update)
        self.btn_save.clicked.connect(self._update_note)

        self.btn_edit.setIcon(QgsApplication.getThemeIcon("mActionToggleEditing.svg"))
        self.btn_cancel.setIcon(
            QgsApplication.getThemeIcon("mActionCancelAllEdits.svg")
        )
        self.btn_save.setIcon(QgsApplication.getThemeIcon("mActionSaveAllEdits.svg"))

    def _update_mode(self) -> None:
        """Switch to UPDATE mode"""
        self.set_display_mode(NoteWidget.DisplayMode.UPDATE)

    def _cancel_update(self) -> None:
        """Cancel UPDATE mode and restore displayed note"""
        if self.note:
            self.set_note(self.note)
        self.set_display_mode(NoteWidget.DisplayMode.READ)

    def _update_note(self) -> None:
        """Update note with current displayed values"""
        if self.note:
            self.note.body = self.txt_body.toPlainText()
            issue_manager = IssuesRequestManager()
            issue_manager.update_issue_note(
                project_id=self.note.project_id,
                issue_iid=self.note.issue_iid,
                note_id=self.note.id,
                body=self.note.body,
            )
            self.set_note(self.note)
            self.set_display_mode(NoteWidget.DisplayMode.READ)
            self.noteUpdated.emit()

    def set_display_mode(self, mode: DisplayMode) -> None:
        """Set display mode:
            NoteWidget.DisplayMode.READ : all GUI are read only
            NoteWidget.DisplayMode.UPDATE : user can edit GUI

        :param mode: display mode
        :type mode: DisplayMode
        """
        self.display_mode = mode
        # Read only attribute
        read_only = mode == NoteWidget.DisplayMode.READ
        self.txt_body.setReadOnly(read_only)
        self.btn_edit.setVisible(mode == NoteWidget.DisplayMode.READ)
        self.btn_save.setVisible(mode == NoteWidget.DisplayMode.UPDATE)
        self.btn_cancel.setVisible(mode == NoteWidget.DisplayMode.UPDATE)

    def set_note(self, note: Note) -> None:
        """Set note displayed

        :param note: note
        :type note: Note
        """
        self.note = note
        self.txt_body.setText(note.body)
        self.txt_body.set_project_id(note.project_id)
        self.lbl_name.setText(note.author.name)
        self.lbl_user_name.setText(f"@{note.author.username}")
        self.lbl_date.setText(
            localize_qdatetime(
                to_qdatetime(note.created_at),
            )
        )
        self.lbl_date.setToolTip(
            localize_qdatetime(to_qdatetime(note.created_at), date_format="long")
        )
