#! python3  # noqa: E265

"""
    Plugin settings form integrated into QGIS 'Options' menu.
"""


# standard
import platform
from functools import partial
from pathlib import Path
from urllib.parse import quote

# PyQGIS
from qgis.core import Qgis, QgsApplication
from qgis.gui import QgsOptionsPageWidget, QgsOptionsWidgetFactory
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QMessageBox

# project
from oslandia.__about__ import (
    __icon_path__,
    __title__,
    __uri_homepage__,
    __uri_tracker__,
    __version__,
)
from oslandia.gitlab_api.custom_exceptions import UnavailableUserException
from oslandia.gitlab_api.user import UserRequestManager
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.cache_manager import CacheManager
from oslandia.toolbelt.preferences import PlgSettingsStructure

# ############################################################################
# ########## Globals ###############
# ##################################

FORM_CLASS, _ = uic.loadUiType(Path(__file__).parent / f"{Path(__file__).stem}.ui")


report_context_message = quote(
    "> Reported from plugin settings\n\n"
    f"- operating system: {platform.system()} "
    f"{platform.release()}_{platform.version()}\n"
    f"- QGIS: {Qgis.QGIS_VERSION}\n"
    f"- plugin version: {__version__}\n"
)

# ############################################################################
# ########## Classes ###############
# ##################################


class ConfigOptionsPage(FORM_CLASS, QgsOptionsPageWidget):
    """Settings form embedded into QGIS 'options' menu."""

    def __init__(self, parent):
        super().__init__(parent)
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager()
        self.plg_settings_get = PlgOptionsManager.get_plg_settings()
        self.cache_manager = CacheManager(".oslandia", "cache")

        # load UI and set objectName
        self.setupUi(self)
        self.setObjectName(f"mOptionsPage{__title__}")

        self.initGui()

    def initGui(self):
        """Set up UI elements."""
        # header
        self.lbl_title.setText(f"{__title__} - Version {__version__}")

        # -- Support customers group
        self.btn_check_authorization.pressed.connect(self.on_check_authorization)
        self.btn_check_authorization.setIcon(
            QIcon(":/images/themes/default/repositoryDisabled.svg")
        )
        self.btn_authentication_help.setIcon(
            QIcon(QgsApplication.iconPath("mActionHelpContents.svg"))
        )
        self.btn_authentication_help.pressed.connect(
            partial(
                QDesktopServices.openUrl,
                QUrl(f"{__uri_homepage__}usage/authentication.html"),
            )
        )

        # -- News Feed
        self.btn_reset_feed_history.pressed.connect(self.on_reset_read_history)

        # -- Miscellaneous group
        self.btn_help.setIcon(QIcon(QgsApplication.iconPath("mActionHelpContents.svg")))
        self.btn_help.pressed.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )
        self.btn_report.setIcon(
            QIcon(QgsApplication.iconPath("console/iconSyntaxErrorConsole.svg"))
        )
        self.btn_report.pressed.connect(
            partial(
                QDesktopServices.openUrl,
                QUrl(
                    f"{__uri_tracker__}/new?issuable_template=bug_report&"
                    "issue[title]=[BUG]&"
                    f"issue[description]={report_context_message}"
                ),
            )
        )
        self.btn_clear_cache.setIcon(
            # QgsApplication.getThemeIcon("iconClearConsole.svg")
            QIcon(":images/themes/default/console/iconClearConsole.svg")
        )
        self.btn_clear_cache.pressed.connect(partial(self.cache_manager.clear_cache))

        self.btn_reset.setIcon(QIcon(QgsApplication.iconPath("mActionUndo.svg")))
        self.btn_reset.pressed.connect(self.on_reset_settings)

        # load previously saved settings
        self.load_settings()

    def apply(self):
        """Called to permanently apply the settings shown in the options page (e.g. \
        save them to QgsSettings objects). This is usually called when the options \
        dialog is accepted."""
        settings = self.plg_settings.get_plg_settings()

        # Authentication
        settings.authentification_config_id = self.acs_auth_select.configId()

        # News feed
        settings.notify_push_info = self.chb_notification_feed_new_content.isChecked()
        settings.notify_push_duration = self.sbx_notif_duration.value()
        settings.rss_poll_frequency_hours = self.sbx_feed_poll_frequency.value()

        # misc
        settings.debug_mode = self.opt_debug.isChecked()
        settings.version = __version__

        # dump new settings into QgsSettings
        self.plg_settings.save_from_object(settings)

        self.wdg_issue_context_auth.save_settings()

        if __debug__:
            self.log(
                message="DEBUG - Settings successfully saved.",
                log_level=4,
            )

    def load_settings(self):
        """Load options from QgsSettings into UI form."""
        settings = self.plg_settings.get_plg_settings()

        # Auth
        self.acs_auth_select.setConfigId(settings.authentification_config_id)
        # News feed
        self.lne_feed_url.setText(settings.rss_source)
        self.chb_notification_feed_new_content.setChecked(settings.notify_push_info)
        self.sbx_feed_poll_frequency.setValue(settings.rss_poll_frequency_hours)
        self.sbx_notif_duration.setValue(settings.notify_push_duration)

        # Misc
        self.opt_debug.setChecked(settings.debug_mode)
        self.lbl_version_saved_value.setText(settings.version)

        self.wdg_issue_context_auth.load_settings()

    def on_reset_read_history(self):
        """Set latest_content_guid to None."""
        new_settings = PlgSettingsStructure(
            latest_content_guid="",
        )

        # dump new settings into QgsSettings
        self.plg_settings.save_from_object(new_settings)

        # inform end user
        self.log(
            message=self.tr("Read history has been reset."),
            log_level=3,
            duration=2,
            push=True,
            parent_location=self,
        )

    def on_reset_settings(self):
        """Reset settings to default values (set in preferences.py module)."""
        default_settings = PlgSettingsStructure()

        # dump default settings into QgsSettings
        self.plg_settings.save_from_object(default_settings)

        # update the form
        self.load_settings()

    def on_check_authorization(self) -> None:
        """
        Check connection by getting user information from API gitlab
        """
        config_id = self.acs_auth_select.configId()
        # Check connection to API by getting token user information
        try:
            manager = UserRequestManager()
            manager.plg_settings.authentification_config_id = config_id
            user = manager.get_current_user()

            self.btn_check_authorization.setIcon(
                QIcon(":/images/themes/default/repositoryConnected.svg")
            )
            self.btn_check_authorization.setToolTip("Connection OK")

            QMessageBox.information(
                self,
                self.tr("Welcome"),
                self.tr(f"Welcome {user.name}!"),
            )

        except UnavailableUserException as exc:
            self.btn_check_authorization.setIcon(
                QIcon(":/images/themes/default/repositoryUnavailable.svg")
            )
            self.btn_check_authorization.setToolTip(str(exc))

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.
        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)


class PlgOptionsFactory(QgsOptionsWidgetFactory):
    """Factory for options widget."""

    def __init__(self):
        """Constructor."""
        super().__init__()

    def icon(self) -> QIcon:
        """Returns plugin icon, used to as tab icon in QGIS options tab widget.

        :return: _description_
        :rtype: QIcon
        """
        return QIcon(str(__icon_path__))

    def createWidget(self, parent) -> ConfigOptionsPage:
        """Create settings widget.

        :param parent: Qt parent where to include the options page.
        :type parent: QObject

        :return: options page for tab widget
        :rtype: ConfigOptionsPage
        """
        return ConfigOptionsPage(parent)

    def title(self) -> str:
        """Returns plugin title, used to name the tab in QGIS options tab widget.

        :return: plugin title from about module
        :rtype: str
        """
        return __title__

    def helpId(self) -> str:
        """Returns plugin help URL.

        :return: plugin homepage url from about module
        :rtype: str
        """
        return __uri_homepage__
