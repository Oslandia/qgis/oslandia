from qgis.PyQt.QtCore import QModelIndex, QObject, QSortFilterProxyModel, Qt

from oslandia.gui.mdl_project import ProjectListModel


class ProjectProxyModel(QSortFilterProxyModel):
    def __init__(self, parent: QObject = None):
        """Proxy model for project

        :param parent: object parent, defaults to None
        :type parent: QObject, optional
        """
        super().__init__(parent)
        self._show_archived = True

    def show_archived(self, show: bool = True) -> None:
        """Define if archived project should be displayed

        :param show: visibility value, defaults to True
        :type show: bool, optional
        """
        if self._show_archived != show:
            self._show_archived = show
            self.invalidateFilter()

    def filterAcceptsRow(self, source_row: int, source_parent: QModelIndex) -> bool:
        """
        Filter visible rows for project

        Args:
            source_row: (int) source row
            source_parent: (QModelIndex) source parent

        Returns: True if row is visible, False otherwise

        """
        result = True
        if not self._show_archived:
            name_index = self.sourceModel().index(
                source_row, ProjectListModel.NAME_COL, source_parent
            )
            project = self.sourceModel().data(name_index, Qt.UserRole)
            if project:
                result = not project.archived

        return result
