from typing import Dict, List, Optional, Union

from qgis.core import QgsApplication, QgsTask
from qgis.PyQt.QtCore import QAbstractTableModel, QModelIndex, QObject, Qt, QVariant
from qgis.PyQt.QtGui import QColor

from oslandia.gitlab_api.issue import Issues, IssuesRequestManager
from oslandia.toolbelt import PlgLogger
from oslandia.toolbelt.qt_utils import localize_qdatetime, to_qdatetime


class IssuesListModel(QAbstractTableModel):
    CREATED_DATE_COL = 0
    NAME_COL = 1
    STATE_COL = 2
    WEB_URL_COL = 3
    DESCRIPTION_COL = 4

    NB_ITEM_BY_PAGE = 5

    def __init__(self, parent: QObject = None):
        """
        QStandardItemModel for datastore list display

        Args:
            parent: QObject parent
        """
        super().__init__(parent)
        self.log = PlgLogger().log
        self.headers = [
            self.tr("Created"),
            self.tr("Title"),
            self.tr("State"),
            self.tr("URL"),
        ]

        self.nb_row = 0
        self._issue_row_dict: dict[int, Issues] = {}
        self.tasks: List[QgsTask] = []

    def headerData(
        self, section: int, orientation: Qt.Orientation, role: Qt.DisplayRole
    ) -> QVariant:
        """Define data for header, only display role is supported

        :param section: section number (column for Horizontal orientation , row for Vertical orientation)
        :type section: int
        :param orientation: orientation
        :type orientation: Qt.Orientation
        :param role: data role
        :type role: Qt.DisplayRole
        :return: header data
        :rtype: QVariant
        """
        if (
            orientation == Qt.Orientation.Horizontal
            and role == Qt.ItemDataRole.DisplayRole
        ):
            return self.headers[section]
        return None

    def rowCount(self, parent=QModelIndex()) -> int:
        """Define number of row in model

        :param parent: parent, defaults to QModelIndex() not used in case of table
        :type parent: QModelIndex, optional
        :return: number of row
        :rtype: int
        """
        return self.nb_row

    def columnCount(self, parent=QModelIndex()) -> int:
        """Define number of column in model

        :param parent: parent, defaults to QModelIndex() not used in case of table
        :type parent: QModelIndex, optional
        :return: number of column
        :rtype: int
        """
        return len(self.headers)

    def data(self, index: QModelIndex, role=Qt.ItemDataRole.DisplayRole) -> object:
        """Define data for model index and role.
        Use data from cache or display loading information before cache load

        :param index: model index
        :type index: QModelIndex
        :param role: data role, defaults to Qt.ItemDataRole.DisplayRole
        :type role: Qt.ItemDataRole, optional
        :return: model data for index and role
        :rtype: object
        """
        if not index.isValid():
            return None

        row, col = index.row(), index.column()
        if row in self._issue_row_dict:
            issues = self._issue_row_dict[row]
            return self._issue_data(issues, col, role)

        if col == self.NAME_COL and role == Qt.ItemDataRole.DisplayRole:
            return self.tr("Loading...")
        else:
            return None

    def _issue_data(
        self, issues: Issues, col: int, role=Qt.ItemDataRole.DisplayRole
    ) -> object:
        """Return issue model data for a specific column and role

        :param issues: issue to display
        :type issues: Issues
        :param col: column
        :type col: int
        :param role: data role, defaults to Qt.ItemDataRole.DisplayRole
        :type role: Qt.ItemDataRole, optional
        :return: _description_
        :rtype: object
        """
        if role == Qt.DisplayRole:
            res = {
                self.NAME_COL: issues.name,
                self.CREATED_DATE_COL: localize_qdatetime(
                    to_qdatetime(issues.created_date)
                ),
                self.STATE_COL: issues.state,
                self.WEB_URL_COL: f"#{issues.iid}",
                self.DESCRIPTION_COL: issues.description,
            }
            return res.get(col, None)
        if role == Qt.UserRole:
            res = {
                self.NAME_COL: issues,
                self.WEB_URL_COL: issues.web_url,
            }
            return res.get(col, None)

        if role == Qt.ToolTipRole:
            res = {
                self.NAME_COL: issues.description,
                self.CREATED_DATE_COL: localize_qdatetime(
                    to_qdatetime(issues.created_date), date_format="long"
                ),
                self.WEB_URL_COL: issues.web_url,
                self.DESCRIPTION_COL: issues.description,
            }
            return res.get(col, None)

        if role == Qt.BackgroundRole and issues.state != "opened":
            return QColor("whitesmoke")

        return None

    def flags(self, index: QModelIndex):
        """Define flag to disable edition

        :param index: model index
        :type index: QModelIndex
        :return: _description_
        :rtype: _type_
        """
        default_flags = super().flags(index)
        return default_flags & ~Qt.ItemIsEditable  # Disable editing

    def set_project(self, project_id: str) -> None:
        """
        Refresh QStandardItemModel data with current project_id members

        """
        # Remove all tasks
        self.tasks.clear()

        # Clear all data
        self.beginResetModel()
        self._issue_row_dict.clear()

        # Load first page
        result = self._load_issue_page(None, project_id, 1)
        issues: List[Issues] = result["issues"]
        self.nb_row = result["nb_issue"]
        nb_page = result["nb_page"]

        for i, issue in enumerate(issues):
            self.set_row_issue(i, issue)

        # Launch load of other pages in QgsTask
        for page in range(1, nb_page):
            task = QgsTask.fromFunction(
                self.tr(f"Load issue page {page} for project {project_id}"),
                self._load_issue_page,
                on_finished=self._issues_page_loaded,
                project_id=project_id,
                page=page,
            )
            self.tasks.append(task)
            QgsApplication.taskManager().addTask(task)

        self.endResetModel()

    def _load_issue_page(
        self, task: Optional[QgsTask], project_id: str, page: int
    ) -> Dict[str, Union[int, list[Issues]]]:
        """Load issue list for a project for a specific page

        :param task: task used for load, can be None
        :type task: QgsTask, optional
        :param project_id: project id
        :type project_id: str
        :param page: page to load
        :type page: int
        :return: dict with load result
        :rtype: Dict[str, Union[int, list[Issues]]]
        """
        manager = IssuesRequestManager()
        issues, nb_issue, nb_page = manager.get_issues_list_by_page(
            project_id, page=page, limit=self.NB_ITEM_BY_PAGE
        )
        return {
            "page": page,
            "issues": issues,
            "nb_issue": nb_issue,
            "nb_page": nb_page,
        }

    def _issues_page_loaded(
        self, exception, result: Dict[str, Union[int, list[Issues]]]
    ) -> None:
        """Function call after QgsTask finished for issue load

        :param exception: exception raised during task
        :type exception: _type_
        :param result: result of issue load
        :type result: Dict[str, Union[int, list[Issues]]]
        """
        page: int = result["page"]
        issue_list: list[Issues] = result["issues"]
        for i, issues in enumerate(issue_list):
            row = i + page * self.NB_ITEM_BY_PAGE
            self.set_row_issue(row, issues)

    def set_row_issue(self, row: int, issues: Issues) -> None:
        """Define data for a row

        :param row: row
        :type row: int
        :param issues: issue
        :type issues: Issues
        """
        self._issue_row_dict[row] = issues
        self.dataChanged.emit(
            self.index(row, 0), self.index(row, self.columnCount() - 1)
        )
