"""Customize display of issue table view
"""

# standard


# PyQGIS
from qgis.PyQt.QtCore import QAbstractItemModel, QEvent, QModelIndex, QSize, Qt, QUrl
from qgis.PyQt.QtGui import QColor, QDesktopServices, QFontMetrics, QPainter
from qgis.PyQt.QtWidgets import QStyledItemDelegate, QStyleOptionViewItem

# project
from oslandia.gui.mdl_issue import IssuesListModel


class IssueTableViewDelegate(QStyledItemDelegate):
    def __init__(self, parentView=None):
        super().__init__(parentView)

        # We need that to receive mouse move events in editorEvent
        parentView.setMouseTracking(True)

        # Revert the mouse cursor when the mouse isn't over
        # an item but still on the view widget
        parentView.viewportEntered.connect(parentView.unsetCursor)

    def paint(
        self, painter: QPainter, option: QStyleOptionViewItem, index: QModelIndex
    ):
        """Paint current index

        :param painter: painter
        :type painter: QPainter
        :param option: option
        :type option: QStyleOptionViewItem
        :param index: index to paint
        :type index: QModelIndex
        """

        if index.column() == IssuesListModel.STATE_COL:

            # Customizing the background for the "bubble"
            painter.save()

            # Get the text from the index
            text = index.data()

            if text != "opened":
                # Fill the background with the selected color
                painter.setBrush(QColor("whitesmoke"))
                painter.setPen(Qt.NoPen)
                painter.drawRect(option.rect)

            # Calculate the size of the text
            font_metrics = QFontMetrics(option.font)
            text_rect = font_metrics.boundingRect(text)
            text_rect.adjust(0, 0, 10, 0)  # Add some padding for the bubble

            # Calculate the position to center the bubble inside the cell
            bubble_rect = text_rect
            bubble_rect.moveCenter(option.rect.center())

            # Set the bubble color (background)
            if text == "opened":
                bubble_color = QColor("lightgreen")
            else:
                bubble_color = QColor("lightgrey")

            painter.setBrush(bubble_color)
            painter.setPen(Qt.NoPen)

            # Draw a rounded rectangle around the text (bubble)
            painter.drawRoundedRect(bubble_rect, 10, 10)

            # Draw the text inside the bubble
            painter.setPen(Qt.black)
            painter.drawText(bubble_rect, Qt.AlignCenter, text)

            # Restore the painter state
            painter.restore()
        elif index.column() == IssuesListModel.WEB_URL_COL:
            painter.save()

            # Récupérer le texte de l'index
            text = index.data()

            # Configurer le style pour ressembler à un lien (texte bleu et souligné)
            font = option.font
            font.setUnderline(True)
            painter.setFont(font)
            painter.setPen(QColor(0, 0, 255))  # Bleu pour le lien

            # Dessiner le texte comme un lien
            painter.drawText(option.rect, Qt.AlignHCenter | Qt.AlignVCenter, text)
            # Restore the painter state
            painter.restore()
        else:
            super().paint(painter, option, index)

    def editorEvent(
        self,
        event: QEvent,
        model: QAbstractItemModel,
        option: QStyleOptionViewItem,
        index: QModelIndex,
    ):
        """Override editorEvent to open link for web url

        :param event: editor event
        :type event: QEvent
        :param model: current mode
        :type model: QAbstractItemModel
        :param option: options
        :type option: QStyleOptionViewItem
        :param index: model index
        :type index: QModelIndex
        :return: _description_
        :rtype: _type_
        """
        if (
            index.column() == IssuesListModel.WEB_URL_COL
            and event.type() == event.MouseButtonRelease
        ):
            if event.button() == Qt.LeftButton:
                url = index.data(Qt.UserRole)
                QDesktopServices.openUrl(QUrl(url))
                return True
        if event.type() == event.MouseMove:
            if index.column() == IssuesListModel.WEB_URL_COL:
                self.parent().setCursor(Qt.PointingHandCursor)
            else:
                self.parent().unsetCursor()

        return super().editorEvent(event, model, option, index)

    def sizeHint(self, option: QStyleOptionViewItem, index: QModelIndex) -> QSize:
        """Returns the size needed by the delegate to display the item specified by index,
        taking into account the style information provided by option.

        :param option: option
        :type option: QStyleOptionViewItem
        :param index: index to paint
        :type index: QModelIndex
        :return: size hint
        :rtype: QSize
        """
        if index.column() == IssuesListModel.STATE_COL:
            # Adjust the size hint to account for the text and the bubble
            text = index.data()
            font_metrics = QFontMetrics(option.font)
            text_size = font_metrics.size(Qt.TextSingleLine, text)
            return text_size + QSize(20, 10)  # Add padding around the text
        return super().sizeHint(option, index)
