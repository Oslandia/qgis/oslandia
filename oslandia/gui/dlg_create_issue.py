"""Dialog for issue creation
"""

# standard
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox, QWidget
from qgis.utils import OverrideCursor

# project
from oslandia.gitlab_api.group import GroupRequestManager
from oslandia.gitlab_api.issue import IssuesRequestManager
from oslandia.gitlab_api.project import ProjectRequestManager
from oslandia.gui.mdl_project import ProjectListModel
from oslandia.gui.proxy_model_project import ProjectProxyModel
from oslandia.gui.wdg_issue import IssueWidget
from oslandia.toolbelt.network_manager import NetworkRequestsManager
from oslandia.toolbelt.preferences import PlgOptionsManager
from oslandia.toolbelt.qgis_info import (
    get_active_plugin_info,
    get_installed_qgis_info_as_markdown,
)


class CreateIssueDialog(QDialog):
    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        with OverrideCursor(Qt.WaitCursor):
            ui_path = Path(__file__).resolve(True).parent / "dlg_create_issue.ui"
            uic.loadUi(ui_path, self)

            self.setWindowTitle(self.tr("Create issue"))

            self.plg_settings = PlgOptionsManager.get_plg_settings()
            self.ntwk_requester_blk = NetworkRequestsManager()
            self.project_request = ProjectRequestManager()
            self.mdl_project = ProjectListModel(self)

            assistance_group = GroupRequestManager().get_group_from_id(
                self.plg_settings.gitlab_group_id
            )
            if assistance_group:
                self.mdl_project.set_group(assistance_group.id)

            self.proxy_mdl = ProjectProxyModel(self)
            self.proxy_mdl.setSourceModel(self.mdl_project)
            self.cbx_projects.setModel(self.proxy_mdl)
            self.cbx_projects.setModelColumn(ProjectListModel.NAME_COL)
            self.proxy_mdl.show_archived(self.ckb_display_archived.isChecked())
            self.proxy_mdl.setSortCaseSensitivity(Qt.CaseInsensitive)
            self.proxy_mdl.sort(ProjectListModel.NAME_COL, Qt.SortOrder.AscendingOrder)

            self.ckb_display_archived.clicked.connect(self._project_filter_changed)
            self.cbx_projects.currentIndexChanged.connect(self._project_changed)

            self.btn_box.button(QDialogButtonBox.StandardButton.Ok).clicked.connect(
                self.on_btn_submit
            )
            self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setDefault(True)
            self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setText(
                self.tr("Submit")
            )
            self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setEnabled(False)
            self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setToolTip(
                self.tr("Issue not valid.\nPlease fill mandatory parameters.")
            )

            self.wdg_issue_context_auth.load_settings()
            self.wdg_issue.issueChanged.connect(self._issue_changed)
            self.wdg_issue.set_display_mode(IssueWidget.DisplayMode.CREATE)

            # Simulate project changes for project_id definition in widgets
            self._project_changed()
            # make the dialog a window to allow resizing
            self.setWindowFlags(Qt.Window)

    def set_current_project_id(self, project_id: str) -> None:
        """Define current selected project

        :param project_id: project id
        :type project_id: str
        """
        project_row = self.mdl_project.get_project_row(project_id)
        if project_row != -1:
            index = self.proxy_mdl.mapFromSource(
                self.mdl_project.index(project_row, self.mdl_project.NAME_COL)
            )
            self.cbx_projects.setCurrentIndex(index.row())

    def _issue_changed(self) -> None:
        """Update submit button if issue is valid for creation"""
        with OverrideCursor(Qt.WaitCursor):
            if not self.wdg_issue.get_issue().valid_for_creation():
                self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setEnabled(
                    False
                )
                self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setToolTip(
                    self.tr("Issue not valid.\nPlease fill mandatory parameters.")
                )
            else:
                self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setEnabled(True)
                self.btn_box.button(QDialogButtonBox.StandardButton.Ok).setToolTip("")

    def _project_filter_changed(self) -> None:
        """Update project filter model"""
        with OverrideCursor(Qt.WaitCursor):
            self.proxy_mdl.show_archived(self.ckb_display_archived.isChecked())

    def _project_changed(self) -> None:
        """Update issue widget for current project"""
        with OverrideCursor(Qt.WaitCursor):
            self.wdg_issue.set_current_project_id(self._selected_project_id())

    def _selected_project_id(self) -> str:
        """Get selected project id

        :return: selected project id
        :rtype: str
        """
        project_index = self.cbx_projects.currentIndex()
        return self.mdl_project.data(
            self.proxy_mdl.mapToSource(
                self.proxy_mdl.index(project_index, ProjectListModel.ID)
            )
        )

    def on_btn_submit(self) -> None:
        """Create issue"""
        self.wdg_issue_context_auth.save_settings()

        issue = self.wdg_issue.get_issue()

        description = issue.description

        if self.wdg_issue_context_auth.is_send_qgis_info_enabled():
            description += f"\n### {self.tr('QGIS Information')}\n"
            description += get_installed_qgis_info_as_markdown()

        if self.wdg_issue_context_auth.is_send_plugin_info_enabled():
            description += f"\n### {self.tr('Installed plugins informations')}\n"
            active_plugin_info = get_active_plugin_info()
            for plugin_name, version in active_plugin_info.items():
                description += f"\n * {plugin_name} : {version}"

        issue.description = description

        issue_manager = IssuesRequestManager()
        issue_manager.create_issue(self._selected_project_id(), issue)
