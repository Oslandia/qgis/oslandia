# standard
import json
from dataclasses import asdict, dataclass, field
from pathlib import Path

import oslandia.toolbelt.log_handler as log_hdlr

QGS_REQUIRED_FIELDS = [
    "accessMethod",
    "grantFlow",
    "configType",
    "clientId",
    "clientSecret",
    "requestUrl",
    "tokenUrl",
    "redirectPort",
    "redirectUrl",
    "persistToken",
    "requestTimeout",
    "version",
    "scope",
]


@dataclass
class OAuth2Configuration:
    accessMethod: int = 0
    apiKey: str = ""
    clientId: str = ""
    clientSecret: str = ""
    configType: int = 1
    customHeader: str = ""
    description: str = (
        "Authentication related to the Oslandia plugin to give access to the GitLab "
        "ticketing and helpdesk to end-customers who subscribed to Oslandia's support offer ."
    )
    grantFlow: int = 0
    id: str = ""
    name: str = "oslandia_plugin_cfg"
    objectName: str = ""
    password: str = ""
    persistToken: bool = True
    queryPairs: dict = field(default_factory=dict)
    redirectPort: int = 7070
    redirectUrl: str = "callback"
    refreshTokenUrl: str = ""
    requestTimeout: int = 30
    requestUrl: str = "https://git.oslandia.net/oauth/authorize"
    scope: str = "api"
    tokenUrl: str = "https://git.oslandia.net/oauth/token"
    username: str = ""
    version: int = 1

    @classmethod
    def is_json_compliant(cls, json_path: Path) -> tuple[bool, list[str]]:
        """
        Checks whether the JSON configuration at the given path contains all the keys required
        for the OAuth2Configuration dataclass.

        :param json_path: Path to the JSON file containing the configuration.
        :type json_path: Path
        :return: A tuple indicating whether the JSON is compliant and a list of missing fields.
        :rtype: tuple[bool, list[str]]
        """
        if not json_path.exists():
            log_hdlr.PlgLogger.log(
                message="The configuration file can be found at this path: {}".format(
                    json_path
                ),
                log_level=1,
                push=True,
            )
            return False, []

        with json_path.open(mode="r", encoding="utf8") as my_json:
            try:
                data = json.load(my_json)
            except json.decoder.JSONDecodeError:
                log_hdlr.PlgLogger.log(
                    message="The json structure {} is incorrect, please check your Oauth2 configuration file.".format(
                        json_path
                    ),
                    log_level=1,
                    push=True,
                )
                return False, []

        missing_fields = [field for field in QGS_REQUIRED_FIELDS if field not in data]

        if missing_fields:
            return False, missing_fields

        return True, []

    def as_qgs_str_config_map(self) -> str:
        """
        Converts the OAuth2Configuration instance to a string representation of a dictionary
        containing only the required fields for the QGIS configuration.

        :return: String representation of the configuration with only required fields.
        :rtype: str
        """
        d = asdict(self)
        keys_to_remove = [key for key in d if key not in QGS_REQUIRED_FIELDS]

        for key in keys_to_remove:
            d.pop(key)

        return str(d)

    @classmethod
    def from_json(cls, json_path: Path):
        """
        Loads an OAuth2Configuration instance from the JSON file at the given path.

        :param json_path: Path to the JSON file containing the configuration.
        :type json_path: Path
        :return: An instance of OAuth2Configuration populated with the data from the JSON file.
        :rtype: OAuth2Configuration
        """
        if not json_path.exists():
            log_hdlr.PlgLogger.log(
                message="The configuration file can be found at this path: {}".format(
                    json_path
                ),
                log_level=1,
                push=True,
            )
            return

        with json_path.open(mode="r", encoding="utf8") as my_json:
            try:
                data = json.load(my_json)
            except json.decoder.JSONDecodeError:
                log_hdlr.PlgLogger.log(
                    message="The json structure {} is incorrect, please check your Oauth2 configuration file.".format(
                        json_path
                    ),
                    log_level=1,
                    push=True,
                )
                return None

        log_hdlr.PlgLogger.log(
            f"oAuth2 configuration loaded from {json_path}", log_level=3
        )

        return cls(
            **data,
        )
