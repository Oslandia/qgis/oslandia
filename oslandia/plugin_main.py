#! python3  # noqa: E265

"""
    Main plugin module.
"""


# standard
from functools import partial
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.core import QgsApplication, QgsSettings
from qgis.gui import QgisInterface, QgsGui
from qgis.PyQt.QtCore import QCoreApplication, QLocale, Qt, QTranslator, QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QAction, QToolBar
from qgis.utils import OverrideCursor

# project
from oslandia.__about__ import (
    DIR_PLUGIN_ROOT,
    __icon_path__,
    __title__,
    __uri_homepage__,
)
from oslandia.gitlab_api.custom_exceptions import UnavailableUserException
from oslandia.gitlab_api.user import UserRequestManager

# PyQGIS
from oslandia.gui.dlg_authentication import AuthenticationDialog
from oslandia.gui.dlg_create_issue import CreateIssueDialog
from oslandia.gui.dlg_plugins_browser import PluginsBrowser
from oslandia.gui.dlg_settings import PlgOptionsFactory
from oslandia.gui.dlg_user import UserDialog
from oslandia.gui.dlg_view_issues import ViewIssueDialog
from oslandia.newsfeed.rss_reader import RssReader
from oslandia.toolbelt import PlgLogger, PlgOptionsManager
from oslandia.toolbelt.plugin_manager import PluginManager

# ############################################################################
# ########## Classes ###############
# ##################################


class OslandiaPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.wizard = None
        self.log = PlgLogger().log

        # -- Dialogs
        self.dlg_auth: Optional[AuthenticationDialog] = None
        self.dlg_create_issue: Optional[CreateIssueDialog] = None
        self.dlg_oslandia_projects: Optional[PluginsBrowser] = None
        self.dlg_user: Optional[UserDialog] = None
        self.dlg_view_issue: Optional[ViewIssueDialog] = None

        # -- Submodules
        self.plg_settings = PlgOptionsManager()
        self.plugin_manager = PluginManager()
        self.rss_reader = RssReader()

        # -- Actions
        self.action_connect = None
        self.action_create_issue = None
        self.action_help = None
        self.action_settings = None
        self.action_user = None
        self.action_view_issue = None

        # translation
        # initialize the locale
        self.locale: str = QgsSettings().value("locale/userLocale", QLocale().name())[
            0:2
        ]
        locale_path: Path = (
            DIR_PLUGIN_ROOT
            / "resources"
            / "i18n"
            / f"{__title__.lower()}_{self.locale}.qm"
        )
        self.log(message=f"Translation: {self.locale}, {locale_path}", log_level=4)
        if locale_path.exists():
            self.translator = QTranslator()
            self.translator.load(str(locale_path.resolve()))
            QCoreApplication.installTranslator(self.translator)

        # plugin settings
        self.toolbar_oslandia: Optional[QToolBar] = None

    def initGui(self):
        """Set up plugin UI elements."""
        self.iface.initializationCompleted.connect(self.post_ui_init)

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        self.action_help = QAction(
            QgsApplication.getThemeIcon("mActionHelpContents.svg"),
            self.tr("Help"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(currentPage=f"mOptionsPage{__title__}")
        )

        self.action_create_issue = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources/images/create_issue.png")),
            self.tr("Create issue"),
            self.iface.mainWindow(),
        )

        self.action_create_issue.triggered.connect(self._create_issue)

        self.action_view_issue = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources/images/view_issue.png")),
            self.tr("View issue"),
            self.iface.mainWindow(),
        )

        self.action_display_projects_browser = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources/images/plugin.png")),
            self.tr("Plugins by Oslandia"),
            self.iface.mainWindow(),
        )

        self.action_view_issue.triggered.connect(self._view_issue)

        self.action_connect = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources/images/connect.png")),
            self.tr("Connect"),
            self.iface.mainWindow(),
        )

        self.action_connect.triggered.connect(self.display_auth)

        self.action_user = QAction(
            QgsApplication.getThemeIcon("user.svg"),
            self.tr("User"),
            self.iface.mainWindow(),
        )

        self.action_user.triggered.connect(self.display_user)

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_connect)
        self.iface.addPluginToMenu(__title__, self.action_user)
        self.iface.addPluginToMenu(__title__, self.action_create_issue)
        self.iface.addPluginToMenu(__title__, self.action_view_issue)
        self.iface.addPluginToMenu(__title__, self.action_display_projects_browser)
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        # -- Toolbar
        if self.toolbar_oslandia is None:
            self.toolbar_oslandia = QToolBar("OslandiaToolbar", self.iface.mainWindow())
            self.toolbar_oslandia.setObjectName("OslandiaToolbar")
        self.iface.addToolBar(self.toolbar_oslandia)
        self.toolbar_oslandia.addAction(self.action_connect)
        self.toolbar_oslandia.addAction(self.action_user)
        self.toolbar_oslandia.addAction(self.action_view_issue)
        self.toolbar_oslandia.addAction(self.action_create_issue)
        self.toolbar_oslandia.addAction(self.action_display_projects_browser)

        # documentation
        self.iface.pluginHelpMenu().addSeparator()
        self.action_help_plugin_menu_documentation = QAction(
            QIcon(str(__icon_path__)),
            f"{__title__} - Documentation",
            self.iface.mainWindow(),
        )
        self.action_help_plugin_menu_documentation.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.iface.pluginHelpMenu().addAction(
            self.action_help_plugin_menu_documentation
        )

        self.action_display_projects_browser.triggered.connect(
            self.display_oslandia_plugins_dialog
        )

        self.action_user.setVisible(False)
        self.action_create_issue.setVisible(False)
        self.action_view_issue.setVisible(False)
        instance_gui = QgsGui.instance()
        instance_gui.optionsChanged.connect(self._check_available_actions)

    def _check_available_actions(self) -> None:
        """Define available actions from current connection"""
        # Check connection to API by getting token user information
        try:
            if not self.plg_settings.get_plg_settings().authentification_config_id:
                self.log(
                    message=self.tr(
                        "Authentication configuration ID is not set in settings. Please use the authenticatio dialog to pair your GitLab account."
                    )
                )
                raise UnavailableUserException
            manager = UserRequestManager()
            manager.get_current_user()
            self.action_connect.setVisible(False)
            self.action_user.setVisible(True)
            self.action_create_issue.setVisible(True)
            self.action_view_issue.setVisible(True)
        except UnavailableUserException:
            self.action_connect.setVisible(True)
            self.action_user.setVisible(False)
            self.action_create_issue.setVisible(False)
            self.action_view_issue.setVisible(False)

    def _create_issue(self):
        """Display create issue dialog"""
        if not self.dlg_create_issue:
            self.dlg_create_issue = CreateIssueDialog()
            self.dlg_create_issue.setModal(False)
        self.dlg_create_issue.show()

    def _view_issue(self):
        """Display view issue dialog"""
        if not self.dlg_view_issue:
            self.dlg_view_issue = ViewIssueDialog()
            self.dlg_view_issue.setModal(False)
        self.dlg_view_issue.show()

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)
        self.iface.removePluginMenu(__title__, self.action_connect)
        self.iface.removePluginMenu(__title__, self.action_user)
        self.iface.removePluginMenu(__title__, self.action_create_issue)
        self.iface.removePluginMenu(__title__, self.action_view_issue)
        self.iface.removePluginMenu(__title__, self.action_display_projects_browser)
        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # remove from QGIS help/extensions menu
        if self.action_help_plugin_menu_documentation:
            self.iface.pluginHelpMenu().removeAction(
                self.action_help_plugin_menu_documentation
            )

        # remove actions
        del self.action_settings
        del self.action_help

        # remove toolbar :
        self.toolbar_oslandia.deleteLater()

    def post_ui_init(self):
        """Things to do when QGIS ui is completed, without blocking QGIS
            initialization (First Content Pain).

        Becareful, this signal is only emitted when the interface is initialized, not
        refreshed or modified. For example, it's not emitted when the plugin is
        manually activated through the extensions manager and nor when another
        processing modify the QGIS GUI.
        """
        self.plugin_manager.update_json_plugins()
        self.rss_reader.process()
        self._check_available_actions()

    def download_icons_and_json_if_needed(self):
        """Download icons and json to cache if not exists"""

        with OverrideCursor(Qt.WaitCursor):
            if not self.plugin_manager.json_exists:
                self.post_ui_init()
            self.plugin_manager.download_plugins_icons_to_cache()

    def display_auth(self) -> None:
        """Display authentication dialog to initiate log-in pairing on GitLab."""
        if self.dlg_auth is None:
            self.dlg_auth = AuthenticationDialog()

        self.dlg_auth.finished.connect(self._check_available_actions)
        self.dlg_auth.show()

    def display_oslandia_plugins_dialog(self) -> None:
        """Display instance duckdb add layer dialog"""
        self.download_icons_and_json_if_needed()

        if self.dlg_oslandia_projects is None:
            self.dlg_oslandia_projects = PluginsBrowser()

        self.dlg_oslandia_projects.show()

    def display_user(self) -> None:
        """Display user dialog."""
        if self.dlg_user is None:
            self.dlg_user = UserDialog()

        self.dlg_user.user_disconnected.connect(self._check_available_actions)

        self.dlg_user.show()
