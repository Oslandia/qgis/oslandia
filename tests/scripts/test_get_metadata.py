#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash
        # for whole tests
        python -m unittest tests.unit.test_get_plugins_oslandia
        # for specific test
        python -m unittest tests.unit.test_get_plugins_oslandia.TestGetPluginsOslandia.test_global
"""


# standard library
import unittest

import requests

# project
from scripts.get_metadata import PluginMetadataRetriever

# ################################
# ########## Classes #############
# ################################


class TestPluginMetadataRetriever(unittest.TestCase):
    def __init__(self, methodName: str = "Test Get Metadata") -> None:
        super().__init__(methodName)

        self.http_session = requests.session()
        self.metadata_gitlab = PluginMetadataRetriever(
            self.http_session, "https://gitlab.com/Oslandia/qgis/qduckdb/"
        )

        self.metadata_github = PluginMetadataRetriever(
            self.http_session,
            "https://github.com/aeag/MenuFromProject-Qgis-Plugin/",
        )

    def test_get_default_branch(self):
        # Gitlab
        self.assertEqual(
            self.metadata_gitlab.get_default_branch,
            "main",
        )

        # Github
        self.assertEqual(
            self.metadata_github.get_default_branch,
            "master",
        )

    def test_file_exists_in_branch(self):
        self.assertTrue(self.metadata_gitlab.file_exists_in_branch("setup.cfg"))
        self.assertFalse(self.metadata_gitlab.file_exists_in_branch("toto.file"))

    def test_find_plugin_path_in_setup_cfg(self):

        self.assertEqual(
            self.metadata_gitlab.find_plugin_path_in_setup_cfg(
                self.metadata_gitlab.build_git_raw_file_url("setup.cfg")
            ),
            "qduckdb",
        )

    def test_build_git_raw_file_url(self):

        expected = "https://raw.githubusercontent.com/aeag/MenuFromProject-Qgis-Plugin/refs/heads/master/setup.cfg"

        self.assertEqual(
            self.metadata_github.build_git_raw_file_url("setup.cfg"), expected
        )

        expected = "https://gitlab.com/Oslandia/qgis/qduckdb/-/raw/main/README.md?ref_type=heads"

        self.assertEqual(
            self.metadata_gitlab.build_git_raw_file_url("README.md"), expected
        )

    def test_get_metadata(self):
        expected_keys = [
            "name",
            "version",
            "about",
            "description",
            "tags",
            "repository",
            "homepage",
            "experimental",
            "icon",
        ]

        metadata = self.metadata_gitlab.get_metadata()

        for key in expected_keys:
            self.assertIn(key, metadata)
            self.assertNotIn(metadata[key], (None, ""))

    def test_get_gitlab_instance(self):
        gitlab_com = PluginMetadataRetriever(
            self.http_session, "https://gitlab.com/Oslandia/qgis/oslandia"
        )
        gitlab_other = PluginMetadataRetriever(
            self.http_session, "https://gitlab.pam-retd.fr/otm/q4ts/"
        )

        self.assertEqual(gitlab_com._detect_gitlab_instance, "https://gitlab.com")
        self.assertEqual(
            gitlab_other._detect_gitlab_instance, "https://gitlab.pam-retd.fr"
        )
