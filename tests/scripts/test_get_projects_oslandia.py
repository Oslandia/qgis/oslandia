#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash
        # for whole tests
        python -m unittest tests.unit.test_get_plugins_oslandia
        # for specific test
        python -m unittest tests.unit.test_get_plugins_oslandia.TestGetPluginsOslandia.test_global
"""


# standard library
import json
import unittest
from pathlib import Path

# project
from scripts import get_projets_oslandia

# ############################################################################
# ########## Classes #############
# ################################


class TestGetPluginsOslandia(unittest.TestCase):
    def validate_plugins_structure(self, json_data):
        """Utility method to validate the structure of plugins in a JSON data."""
        self.assertTrue(json_data)
        self.assertTrue(
            all(
                isinstance(plugin, dict) and "name" in plugin and "version" in plugin
                for plugin in json_data.get("public_plugins", [])
            )
        )

    def test_build_json_from_xml_and_metadata(self):
        json_from_xml = get_projets_oslandia.build_json_from_xml_and_medata(
            get_projets_oslandia.get_xml_plugin()
        )
        self.validate_plugins_structure(json_from_xml)

    def test_build_json_from_gitlab_public_and_metadata(self):
        json_from_gitlab_public = (
            get_projets_oslandia.build_json_from_gitlab_public_and_metadata()
        )
        self.validate_plugins_structure(json_from_gitlab_public)

    def test_build_json_private_plugins(self):
        json_from_private_plugin = (
            get_projets_oslandia.build_json_from_private_and_medatada(
                Path("./oslandia/static/json_plugins/private_plugins.json")
            )
        )
        self.validate_plugins_structure(json_from_private_plugin)

    def setUp(self):
        """
        Method executed before each test.
        Data and a temporary file are prepared for testing.
        """
        get_projets_oslandia.set_http_session()
        self.test_data = {"name": "Alice", "age": 30, "city": "Paris"}
        self.test_file = Path("test_data.json")

    def tearDown(self):
        """
        Method performed after each test.
        Files created are cleaned to avoid residue.
        """

        if self.test_file.exists():
            self.test_file.unlink()

    def test_save_to_json(self):
        """
        Tests whether the save_to_json method correctly saves data.
        """

        get_projets_oslandia.save_to_json(self.test_data, self.test_file)
        self.assertTrue(self.test_file.exists())
        with self.test_file.open(encoding="utf-8") as f:
            loaded_data = json.load(f)
        self.assertEqual(self.test_data, loaded_data)
