#! python3  # noqa E265

"""
Usage from the repo root folder:

.. code-block:: bash
    # for whole tests
    python -m unittest tests.unit.test_oauth2_configuration
    # for specific test
    python -m unittest tests.qgis.test_oauth2_configuration.TestOauth2Configuration.test_init_class
"""


# standard library
import unittest
from pathlib import Path

# project
from oslandia.datamodels.oauth2_configuration import OAuth2Configuration

# ############################################################################
# ########## Classes #############
# ################################


class TestOauth2Configuration(unittest.TestCase):
    """Test utils related to OAuth2Configuration."""

    def test_init_class(self):
        data = OAuth2Configuration()
        excepted = OAuth2Configuration(
            accessMethod=0,
            apiKey="",
            clientId="",
            clientSecret="",
            configType=1,
            customHeader="",
            description="Authentication related to the Oslandia plugin to give access to the GitLab ticketing and helpdesk to end-customers who subscribed to Oslandia's support offer .",
            grantFlow=0,
            id="",
            name="oslandia_plugin_cfg",
            objectName="",
            password="",
            persistToken=True,
            queryPairs={},
            redirectPort=7070,
            redirectUrl="callback",
            refreshTokenUrl="",
            requestTimeout=30,
            requestUrl="https://git.oslandia.net/oauth/authorize",
            scope="api",
            tokenUrl="https://git.oslandia.net/oauth/token",
            username="",
            version=1,
        )
        self.assertEqual(data, excepted)

    def test_from_json_and_qgs_str_config(self):
        json_config = Path("oslandia/auth/oauth2_config.json")
        self.assertTrue(json_config.exists)
        data = OAuth2Configuration.from_json(json_config)
        excepted = str(
            {
                "accessMethod": 0,
                "clientId": None,
                "clientSecret": None,
                "configType": 1,
                "grantFlow": 0,
                "persistToken": False,
                "redirectPort": 7070,
                "redirectUrl": "callback",
                "requestTimeout": 30,
                "requestUrl": "https://git.oslandia.net/oauth/authorize",
                "scope": "api",
                "tokenUrl": "https://git.oslandia.net/oauth/token",
                "version": 1,
            }
        )
        self.assertEqual(data.as_qgs_str_config_map(), excepted)

    def test_is_json_compliant(self):
        self.assertTrue(
            OAuth2Configuration.is_json_compliant(
                Path("oslandia/auth/oauth2_config.json")
            )[0],
        )
