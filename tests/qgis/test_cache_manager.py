#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash

        # for whole tests
        python -m unittest tests.qgis.test_cache_manager
        # for specific test
        python -m unittest tests.qgis.test_cache_manager.TestCacheManager.test_cache_manager_class
"""

# standard library
from qgis.testing import unittest

# project
from oslandia.toolbelt.cache_manager import CacheManager

# ############################################################################
# ########## Classes #############
# ################################


class TestCacheManager(unittest.TestCase):
    def test_cache_manager_class(self):
        """Test method ensure_cache_dir_exists"""

        # Make sure that the oslandia plugin cache does not exist, so that tests run smoothly.
        cache_manager = CacheManager(".qgis", "oslandia")
        cache_manager.clear_cache()

        # Not exists
        cache_manager = CacheManager(".qgis", "toto")
        self.assertFalse(cache_manager.ensure_cache_dir_exists())

        # Not Exists and create
        cache_manager = CacheManager(".qgis", "oslandia")
        self.assertFalse(cache_manager.ensure_cache_dir_exists())
        cache_manager.create_cache_dir()
        self.assertTrue(cache_manager.ensure_cache_dir_exists())

        # Exists and drop
        cache_manager = CacheManager(".qgis", "oslandia")
        self.assertTrue(cache_manager.ensure_cache_dir_exists())
        cache_manager.clear_cache()
        self.assertFalse(cache_manager.ensure_cache_dir_exists())


# ############################################################################
# ####### Stand-alone run ########
# ################################
if __name__ == "__main__":
    unittest.main()
