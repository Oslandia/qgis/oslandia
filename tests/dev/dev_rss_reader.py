from email.utils import parsedate
from xml.etree import ElementTree

import requests

rss_source: str = "https://oslandia.com/feed/"

response = requests.get(rss_source)

tree = ElementTree.fromstring(response.content)

XML_NAMESPACES: dict[str, str] = {
    "atom": "http://www.w3.org/2005/Atom",
    "content": "http://purl.org/rss/1.0/modules/content/",
    "dc": "http://purl.org/dc/elements/1.1/",
    "slash": "http://purl.org/rss/1.0/modules/slash/",
    "sy": "http://purl.org/rss/1.0/modules/syndication/",
    "wfw": "http://wellformedweb.org/CommentAPI/",
}

feed_items = list
items = tree.findall(path="channel/item")
for item in items:
    print(item.find("guid").text)
    # add items to the feed

    dico_item = {
        "abstract": item.find("description").text,
        "author": [author.text for author in item.findall("dc:creator", XML_NAMESPACES)]
        or None,
        "categories": [category.text for category in item.findall("category")] or None,
        "date_pub": parsedate(item.find("pubDate").text),
        "guid": item.find("guid").text,
        "title": item.find("title").text,
        "url": item.find("link").text,
    }

    if item.find("enclosure") is not None:
        item_enclosure = item.find("enclosure")
        dico_item["image_length"] = item_enclosure.attrib.get("length")
        dico_item["image_type"] = item_enclosure.attrib.get("type")
        dico_item["image_url"] = item_enclosure.attrib.get("url")
    print(items.index(item), dico_item)
print(type(parsedate(item.find("pubDate").text)))
# for elem in item.iter():
#     print(elem.tag)

# print(dir(item), item.keys())
