# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.9.0 - 2025-02-25

- UI: adds a new cards view to the Project Explorer (!148, Florent Fougères)
- feature: include projects other than plugins to other_projects.json (!146, Florent Fougères)
- fix: define objectName to save position for next QGIS launch (!148, Jean-Marie Kerloch)

## 0.8.1 - 2025-01-27

- fix(import): QAction is in QtWidgets
- improve(ui): make dialog maximizable by window's icon
- Custom widget to manage project display in the browser and systematic use of dataclass

## 0.8.0 - 2025-01-24

- feature: cache more items and add a button to clear cache folder
- feature: display images in issues
- feature: allow to upload images and files (drag'n drop and file picker) in issue description and comments (!133, !135)
- feature: add a toolbar to help formatting Markdown handling bold, italic, strike and code blocks (!136, !137, !139)
- improved: network manager now logs content size
- improved: logic behind plugins explorer (scripts, quality of code...)
- improved: i18n tooling has now a script to generate automatically the translation files (!140)
- improved: CI/CD workflow (!130)
- minor fixes and improvements

## 0.7.0 - 2025-01-16

- UI improvements: make windows not modal (!106), display short datetime based on application's locale (!101), add button to view profile online (!108)
- global improvements and UI tweaks on the plugins browser (!100, !102, !109, !110, !111, !113, !115)
- improve authentication workflow and add log out mechanism (!118)
- add user avatar (!114)
- add project avatar (!116)
- add a quick filter to the issues browser based on title and description (!117)
- updated translations

## 0.6.2 - 2025-01-13

- Make plugin compatible with QGIS with Qt6 (!72)
- Improvements to the plugin explorer (!94 !95 !96 !98)
- Add GitLab group id to settings and make group retriever more robust (!93)

## 0.6.1 - 2025-01-10

- Improve group retrieval from GitLab API to manage name conflicts for certain users (!93)

## 0.6.0 - 2025-01-10

- Add edition of issue (#12 !63)
- Add edition of issue comments (!86 !87)
- Minor UI update (!88 !89 !90)

## 0.5.1 - 2025-01-09

- Fix incompatible Python syntax with targetted version 3.9

## 0.5.0 - 2024-12-24

- Management of authentication by group rather than individually. (!73 !74 !75 !76)
- Configure Oauth2 authentication with a json conf file provided by the plugin or by an environment variable. (!82 !78)
- Fix and enhance the script that collects information from plugins (!79)

## 0.4.1 - 2024-11-05

- Fix and enhance the scripts that build the jsons describing the plugins (!69 !67 !68)
- improve(ui): move issue context within support customers group (!66)
- add(tooling): use mailmap to curate git log (!70)

## 0.4.0 - 2024-11-05

- feat(plugins_browser): Add a button to force json refresh and display the last update date (!65)

## 0.3.3 - 2024-11-04

- fix(ui): move settings and help actions to the menu's bottom
- update(ui): add French translation

## 0.3.2 - 2024-10-28

- feat(issue): add url for issue in table (!54)
- feat(issue): add button to create issue in issue list (!53)
- feat(issue): add refresh button (!52)
- feat(issue): add new style for issue state (!51)
- feat(issue): issue ui updates (!50)
- fix(issue): invalid issue display if filter enabled (!49)
- fix(api): avoid crash if request reply is empty (!48)

## 0.3.1 - 2024-10-03

- add buttons icons
- move every button within Oslandia toolbar and menu

## 0.3.0 - 2024-09-27

- add UI to explore plugins made by Oslandia
- achieve GitLab authentication integration (oAuth2)
- add widget to list issues per GitLab projects
- add form to create a GitLab issue
- minor fixes and improvements

## 0.2.0 - 2024-08-08

- add some common utils: network manager, application folder...
- add RSS feed parsing logic and UI
- define authentication workflow and implement/document basic integration in QGIS Authentication Manager
- improve CI and tooling

## 0.1.0 - 2024-06-12

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
